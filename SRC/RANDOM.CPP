#include "os.h"
#include "random.h"

Random::Random()
	{
	int i;

	nextIndex = 0;
	for (i = 0; i < RAND_TABLE_SIZE; ++i)
		table[i] = 0;
	}

Random::~Random()
	{
	}

void Random::Seed(int s)
	{
	int i;

	srand(s);
	for (i = 0; i < RAND_TABLE_SIZE; ++i)
		table[i] = rand() % 256;
	nextIndex = 0;
	}

unsigned int Random::GetRandom(unsigned int limit)
	{
	unsigned int returnValue;
	int i;

	returnValue = 0;
	for (i = 0; i < 4; ++i)
		{
		returnValue = (returnValue << 8) + table[nextIndex];
		++nextIndex;
		if (nextIndex >= RAND_TABLE_SIZE)
			nextIndex = 0;
		}
	return (returnValue % limit);
	}

