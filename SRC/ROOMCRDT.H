#ifndef _roomcredits
#define _roomcredits

#include "sciroom.h"
#include "sciview.h"
#include "sciscrip.h"
#include "scibuttn.h"

class RoomCredits : public sciRoom
	{
	public:
		RoomCredits();
		~RoomCredits();
		void Init();
		Boolean HandleEvent(sciEvent &event);

		sciView *credits;
	};

class RoomCreditsScript : public sciScript
	{
	public:
		void ChangeState(int newState);
	protected:		
	};

#endif // _roomcredits
