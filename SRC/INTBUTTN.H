// INTBUTTN.H -- the various interface buttons
//
// TW

#ifndef _intbutton
#define _intbutton

#include "scibtngp.h"
#include "command.h"

#ifndef _VAMPIRE_H_
#include "vampire.h"
#endif

class StatusLine;

typedef enum _BUTTON_ATTR {
	BTN_ATTR_AGGRESSION,
	BTN_ATTR_TENACITY,
	BTN_ATTR_SIGHT,
	BTN_ATTR_ATTACK,
	BTN_ATTR_DEFEND,		
	BTN_ATTR_SPEED,		
	BTN_ATTR_STEALTH,		
	BTN_ATTR_DETECTION,	
	BTN_ATTR_SPELLS,
	BTN_ATTR_LEVEL,
} BUTTON_ATTR;


typedef enum {
	BTN_STATE_UNSELECTED,
	BTN_STATE_SELECTED, 
	BTN_STATE_HILIGHTED,
	BTN_STATE_DEPRESSED,
	BTN_STATE_DISABLED
} BUTTON_STATE;	//the state the button(and its view) can be in at any one time

typedef enum {
	NOT_PENDING,
	PENDING
} PENDING_STATE;

typedef enum _BTN_MODE_ {
	BTN_MOVE,
	BTN_ATTACK,
	BTN_FEED,
	BTN_CONVERT,
	BTN_TAKE,
	BTN_GUARD,
	BTN_CASTSPELL,
	BTN_SCOUT,
	BTN_PATROL,
	BTN_CLOAK,
	BTN_CNTR_PANEL,
	BTN_MAP_ZOOM,
	BTN_DEF_ATTACK,
	BTN_UNDEFINED
} BTN_MODE;

//The display currently seen to the right of the portrait(s)
typedef enum _DISPLAY_MODE_ {
	DISPLAY_NONE,
	DISPLAY_UPGRADES,
	DISPLAY_SPELLS,
	DISPLAY_CNTR_PANEL,
} DISPLAY_MODE;

//Button IDs for the two radio display buttons
enum {
	DISP_BTN_UPGRADES,
	DISP_BTN_SPELLS,
	DISP_BTN_SETTINGS,
};


class UpgradeButton : public sciProp {
	public:
		UpgradeButton(int nButtonID, Boolean bSelected = FALSE);
		~UpgradeButton();

		void Init(int nWidth = 0, int nHeight = 0);
		void vSetPlane(sciPlane *poPlane);
		Boolean HandleEvent(sciEvent &oEvent);
		void vEnable();
		void vDisable();
		Boolean bEnabled() {return mbEnabled;}
		Boolean bHidden() {return hidden;}
		virtual void vDoLeftClick();
		virtual void vDoRightClick();
		void Render(RECT &rUpdateRect);
		void vSetHotkey(char c) {mcHotkey = tolower(c);}
		void vSetHotkey2(char c) {mcHotkey2 = tolower(c);};
		void vSetRightHotkey(char c) {mcRightHotkey = tolower(c);}
		void vSetRepeat(int nStartDelay = -1, int nRepeatDelay = -1);
		friend class UpgradeButtonRepeatScript;
		
		void vSetText(char *pstrText);
		void vSetTextSize() {if (mpoText) mpoText->vSetSize();}
		int nTextHeight() {return mpoText ? mpoText->nHeight() : 0;}
		int nTextWidth() {return mpoText ? mpoText->nWidth() : 0;}
		void vSetFont(int nUnsel, int nSel, int nHi, int nDep, int nDis = -1) ;
		void vSetFontBG(int nPending, int nNonPending);
		void vTextPosn(int nAddX, int nAddY);
		void vSetTextJust(int nJust);
		
		void vSetViews(int nUnselectView, int nSelectView, int nPendingView);
		void vSetButtonCels(int nUnsel, int nSel, int nHi, int nDep, int nDis) ;
		void vSetPri(int nPri);
		void Posn(int nX, int nY);
		void Show();
		void Hide();

		int nGetID() {return mnButtonID;}
		void vSetID(int nID) {mnButtonID = nID;}
		void vSetButtonGroupList(RefList<UpgradeButton> *poList) {mpoButtonGroupList = poList;}

		void vClearPending() {mbPending = FALSE;}
		void vSetPending()	{mbPending = TRUE;}
		void vSelect()	{mbSelected = TRUE; mnState = BTN_STATE_SELECTED; vUpdateCel();}
		void vUnselect() {mbSelected = FALSE; mnState = BTN_STATE_UNSELECTED; vUpdateCel();}

		void vSetSelectionList(RefList<DrawObj> *poUnitList) {mpoSelectionList = poUnitList;}

	protected:
		void vSetAsPending();	//sets self as pending and clears pending of any other button in button list
		void vSetState(int nState);
		void vSetSelectedState();
		void vUpdateCel();
		int mnState;	

		sciText *mpoText;
		char mcHotkey, mcHotkey2;
		char mcRightHotkey;
		int mnStartDelay;
		int mnRepeatDelay;
		int mnJustification;

		Boolean mbMouseDown;
		Boolean mbEnabled;
		Boolean mbSelected;
		Boolean mbPending;


		//text fonts/colors for when we use text buttons instead of bitmap buttons
		int mnPendingBG;		//background text color when upgrade is pending
		int mnNonPendingBG;		//background text color when upgrade is not pending (normal)
		int mnUnselectedFont;	//font when button is ready to have state changed(select, hilight, depressed)
		int mnSelectedFont;		//font when button is currently active(on) for given attribute/skill
		int mnHilightFont;		//font when cursor is over button (but no mouse down)
		int mnDepressedFont;	//font when cursor is over button and mouse is down
		int mnDisabledFont;		//font when cursor is disabled (can't click on it at all)

		
		int mnUnselectedCel;	//cel when button is ready to have state changed(select, hilight, depressed)
		int mnSelectedCel;		//cel when button is currently active(on) for given attribute/skill
		int mnHilightCel;		//cel when cursor is over button (but no mouse down)
		int mnDepressedCel;		//cel when cursor is over button and mouse is down
		int mnDisabledCel;		//cel when cursor is disabled (can't click on it at all)

		//this structure assumes that the 'selected' view will go over self
		//and self's view is the un-selected view. And that 'pending' view goes over selected and unselected views
		sciView *mpoSelectedView;
		sciView *mpoPendingView;


		int		mnButtonID;
		RefList<UpgradeButton> *mpoButtonGroupList;

		RefList<DrawObj> *mpoSelectionList; //a back pointer to the current unit selection list

		static UpgradeButton *mpoCurrentHilight;	//current button that is highlighted

};

class UpgradeButtonGroup : public sciView {
	public:
		UpgradeButtonGroup();
		~UpgradeButtonGroup();
		void vSetStatusLine(StatusLine *poStatusLine) {mpoStatusLine = poStatusLine;}
		void vSetStatusText(char *pstrNewText);
		void vSetGroupRect();
		Boolean HandleEvent(sciEvent &oEvent);
		void vAddToGroup(UpgradeButton *poButton);
		void vDisableAll();
		void vEnableAll();
		void Posn(int nX, int nY);

		//Icon related
		sciView	*mpoIconView;
		sciText *mpoIconText;
		void vSetIconText(char *pstrNewText);
		void vSetIconView(int nNewView);

		void vUpdateButtonState(RefList<DrawObj> *poUnitList);
		void vUpdateButtonState(Vampire *poVampire);
		//void Show(RefList<DrawObj> *poUnitList);
		void Show();
		void Hide();

	protected:
		RefList<UpgradeButton> *mpoButtonList;
		StatusLine	*mpoStatusLine;
		char *mpoStatusText;	//the text that will be display on the status line when mouse is over it
		
};


//Attribute button
class AttributeButton : public UpgradeButton {
	public:
		AttributeButton(BUTTON_ATTR eAttribute, int nButtonID, Boolean bSelected = FALSE);
		~AttributeButton();
		void vDoLeftClick();
		void vUpdateState(Vampire * poUnit);	//setup current state before we 'show' self
		Boolean bCanUpgrade();	//returns F if button won't upgrade given meAttribute, T if it can
		void Show();

		//		void Doit();
	protected:
		BUTTON_ATTR	meAttribute;
};

class CommandButton : public sciButton {
	public:
		CommandButton(BTN_MODE	eMode);
		~CommandButton();
		Boolean HandleEvent(sciEvent &oEvent);
		void DoLeftClick();
		void vSetStatusLine(StatusLine *poStatusLine) {mpoStatusLine = poStatusLine;}
		void vSetStatusText(char *pstrNewText);
	protected:
		BTN_MODE	meMode;
		StatusLine	*mpoStatusLine;
		char *mpoStatusText;	//the text that will be display on the status line when mouse is over it
};

class RadioDisplayButton : public sciRadioButton {
	public:
		RadioDisplayButton(int nID, Boolean bSelected = FALSE);
		~RadioDisplayButton();
		void DoLeftClick();
		Boolean HandleEvent(sciEvent &oEvent);
		void vSetStatusLine(StatusLine *poStatusLine) {mpoStatusLine = poStatusLine;}
		void vSetStatusText(char *pstrNewText);
	protected:
		StatusLine	*mpoStatusLine;
		char *mpoStatusText;	//the text that will be display on the status line when mouse is over it
};


#endif