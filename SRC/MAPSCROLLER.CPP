// MAPSCROLLER.CPP -- the map scroller
//
// TW

#include "scimain.h"
#include "mapscroller.h"
#include "mapplane.h"
#include "statusln.h"
#include "template.h"
#include "minimap.h"

MapScroller::MapScroller(int nPri, Template *poInterfaceTemplate) : sciPlane(pApp->pWindow, FALSE, nPri)
{
	assert(poInterfaceTemplate);
	mpoStatusLine = NULL;
	mpoStatusText = NULL;
	mpoMiniMap = NULL;
	mpoScrollBox = NULL;
	mpoInterfaceTemplate = poInterfaceTemplate;
	vSetStatusText("Scroller");
	

	//init the frame border
	mpoMapFrame = new MapFrame(nPri - 10, poInterfaceTemplate, this);

	mnZoom = game->mpoTemplate->lGetValue(TAG_MMag,1);
	mnPosX=0;
	mnPosY=0;	

	vSetSizeAndPosition();		//setup size and position of plane
	mpoMiniMap->vGameCycle();	//update the map
	vInitScrollBox();
	
	mpoMapFrame->vErase(mpoInterfaceTemplate);
	
}

MapScroller::~MapScroller()
{
	if (mpoStatusText) {
		delete[] mpoStatusText;
		mpoStatusText = NULL;
	}

	//mpoScrollBox is a subclass of sciFeature which gets deleted in sciPlanes destructor
	//so don't need to delete
	mpoScrollBox = NULL;

	if (mpoMiniMap) {
		delete[] mpoMiniMap;
		mpoMiniMap = NULL;
	}

	//no need for delete here as the window disposes of all planes in its list
	mpoMapFrame = NULL;

}

void MapScroller::vSetSizeAndPosition()
{
	//setup initial size and position based on interface templates
	int nWidth = mpoInterfaceTemplate->lGetValue(TAG_MpsW,0);
	int nHeight = mpoInterfaceTemplate->lGetValue(TAG_MpsH,0);
	int nPosX = mpoInterfaceTemplate->lGetValue(TAG_MpsX,0);
	int nPosY = mpoInterfaceTemplate->lGetValue(TAG_MpsY,0);
	//
	//adjust in case map is smaller than scroll region
	if (gpoMap->nGetWidth() * mnZoom < nWidth)
	{
		nPosX += (nWidth - gpoMap->nGetWidth() * mnZoom) / 2;
		nWidth = gpoMap->nGetWidth() * mnZoom;
	}
	if (gpoMap->nGetHeight() * mnZoom < nHeight)
	{
		nPosY += (nHeight - gpoMap->nGetHeight() * mnZoom) / 2;
		nHeight = gpoMap->nGetHeight() * mnZoom;
	}
	//
	//now set size and position of plane
	vSetRect(0, 0, nWidth, nHeight);
	Posn(nPosX, nPosY);

	if (mpoMiniMap)
		delete mpoMiniMap;
	mpoMiniMap = new MiniMap(gpoMap->nGetWidth(), gpoMap->nGetHeight(), mpoMapFrame, mnZoom);
		
//	vSetBackgroundColor(5);

}

void MapScroller::vGameCycle()
{
	mpoMiniMap->vGameCycle();
}

void MapScroller::Doit() {
	if (isNotHidden && mpoScrollBox)
		mpoScrollBox->Doit();
}

void MapScroller::Render (RECT &rUpdateRect) {
	//vDebugOut("x: %d, y:%d\n", 0 - mnPosX, 0 - mnPosY);
	mpoMiniMap->vDraw(0 - mnPosX + (left - mpoMapFrame->GetX()), 0 - mnPosY + (top - mpoMapFrame->GetY()));
	sciPlane::Render(rUpdateRect);
}

//Zooms map in/out (moved from HandleEvent)
void MapScroller::vZoom()
{
			if (mnZoom == 2)
				mnZoom = 1;
			else 
				mnZoom = 2;

//			mnPosX=0;
//			mnPosY=0;	
//			vSetSizeAndPosition();	//setup size and position of plane
//			vInitScrollBox();
			vReAdjust();
			Doit();	//update positions of everything
			mpoMiniMap->vForceUpdate();
}

Boolean MapScroller::HandleEvent(sciEvent & oEvent) 
{
	if (isNotHidden == FALSE) {
		return FALSE;
	}


	switch (oEvent.type) {

/*	case KEY_PRESS: //moved to vZoom()
		if (oEvent.c == VK_TAB)
		{
			if (mnZoom == 2)
				mnZoom = 1;
			else 
				mnZoom = 2;

//			mnPosX=0;
//			mnPosY=0;	
//			vSetSizeAndPosition();	//setup size and position of plane
//			vInitScrollBox();
			vReAdjust();
			Doit();	//update positions of everything
			mpoMiniMap->vForceUpdate();
		}
		break;
*/
	case MOUSE_MOVE:
		//event is localized but planes points are global, so use OnMeGlobalized call
		if (OnMeGlobalized(oEvent.x, oEvent.y)) {
			//vDebugOut("On me\n");
			//vDebugOut("x: %d, y:%d\n", plane->GetCursorX(), plane->GetCursorY());
			assert(mpoStatusLine);
			if (mpoStatusText) {
				mpoStatusLine->vAddToBar((MapScroller*)this, mpoStatusText);			
			}
//			oEvent.claimed = TRUE;
		} else {
			assert(mpoStatusLine);
			if (mpoStatusLine->bOnBar((MapScroller*)this)) {
				mpoStatusLine->vClearFromBar((MapScroller*)this);
			}
		}
		break;
	}

	//call the super
	sciPlane::HandleEvent(oEvent);
	
	//check for single clicks in the area
	if (!oEvent.claimed && OnMeGlobalized(oEvent.x, oEvent.y))
	{
		switch (oEvent.type) {
		case MOUSE_DOWN:
			if (oEvent.modifiers & RIGHT_MOUSE)
			{
				vRightClick(oEvent);
			} else {
				mpoScrollBox->vCenter(oEvent.x, oEvent.y);			
			}
			oEvent.claimed = TRUE;
			break;
		case MOUSE_MOVE:
			oEvent.claimed = TRUE;
			break;
		}
	}

	return oEvent.claimed;

}

void MapScroller::vFrameEvent(sciEvent oEvent)
{
	if (!oEvent.claimed)
	{
		//globalize event(from Frames plane)
		mpoMapFrame->vLocalToGlobal(&oEvent.x, &oEvent.y);
		
		//now localize to this plane
		vGlobalToLocal(&oEvent.x, &oEvent.y);


		switch (oEvent.type) {
		case MOUSE_DOWN:
			if (oEvent.modifiers & RIGHT_MOUSE)
			{
				vRightClick(oEvent);
			} else {
				
				//move scroll box to edge(and thus moving GameMap in process
				//then scroll map to middle of box

				if (oEvent.x < 0) {
					//oEvent.x = 0;
					mpoScrollBox->vCenter(0, oEvent.y);
					gpoMap->bScrollLeft((abs(oEvent.x) + mpoScrollBox->GetWidth() / 2 ) / mnZoom);
				}
				else if (oEvent.x >= mnWidth - 1) {
					//oEvent.x = mnWidth - 1;
					mpoScrollBox->vCenter(mnWidth - 1, oEvent.y);
					gpoMap->bScrollRight((oEvent.x - mnWidth + mpoScrollBox->GetWidth() / 2 ) / mnZoom);
				}
				
				if (oEvent.y < 0) {
//					oEvent.y = 0;
					mpoScrollBox->vCenter(oEvent.x, 0);
					gpoMap->bScrollUp((abs(oEvent.y) + mpoScrollBox->GetHeight() / 2 ) / mnZoom); 
				}
				else if (oEvent.y >= mnHeight) {
					//oEvent.y = mnHeight - 1;
					mpoScrollBox->vCenter(oEvent.x, mnHeight - 1);
					gpoMap->bScrollDown((oEvent.y - mnHeight + mpoScrollBox->GetHeight() / 2 ) / mnZoom);
				}

				//mpoScrollBox->vCenter(oEvent.x, oEvent.y);			
			}
		}
	}
}


void MapScroller::vSetStatusText(char *pstrNewText) {
	int nStrLen;

	if (mpoStatusText && pstrNewText && !strcmp(mpoStatusText, pstrNewText)) {
		return;
	}

	if (mpoStatusText) {
		delete[] mpoStatusText;
	}

	if (pstrNewText) {
		nStrLen = strlen(pstrNewText);
		mpoStatusText = new char[nStrLen + 1];
		strcpy(mpoStatusText, pstrNewText);
	}
}

void MapScroller::vInitScrollBox()
{
	if (mpoScrollBox)
		delete mpoScrollBox;

	mpoScrollBox = new MapScrollerBox((BYTE)mpoInterfaceTemplate->lGetValue(TAG_MpsR,255), 
									  (BYTE)mpoInterfaceTemplate->lGetValue(TAG_MpsG,255), 
									  (BYTE)mpoInterfaceTemplate->lGetValue(TAG_MpsB,255), 
									  mpoInterfaceTemplate->lGetValue(TAG_MpsT,1), mnZoom);
	mpoScrollBox->vSetPlane(this);
	mpoScrollBox->vSetupBitmap();
	mpoScrollBox->Init();
	mpoScrollBox->Show();

}
void MapScroller::vReAdjust()
{
	mnPosX=0;
	mnPosY=0;	
	vSetSizeAndPosition();
	vInitScrollBox();	//and dispose of old scroll box
	//mpoScrollBox->vSetupBitmap();	
}

void MapScroller::vForceUpdate()
{
	mpoMiniMap->vForceUpdate();
}

void MapScroller::vScrollRight(int nHowMuch)
{
	mnPosX = min(gpoMap->nGetWidth() * mnZoom -  gpoMap->nGetScreenWidth() * mnZoom, mnPosX + nHowMuch);
//	mnPosX /= mnZoom;
//	vDebugOut("R mnX:%d mnY%d\n", mnPosX,mnPosY);
}

void MapScroller::vScrollLeft(int nHowMuch)
{
	mnPosX = max(0, mnPosX - nHowMuch);
//	mnPosX /= mnZoom;
//	vDebugOut("L mnX:%d mnY%d\n", mnPosX,mnPosY);
}
void MapScroller::vScrollUp(int nHowMuch)
{
	mnPosY = max(0, mnPosY - nHowMuch);
//	vDebugOut("U mnX:%d mnY%d\n", mnPosX,mnPosY);
}
void MapScroller::vScrollDown(int nHowMuch)
{
	mnPosY = min(gpoMap->nGetHeight() * mnZoom - gpoMap->nGetScreenHeight() * mnZoom, mnPosY + nHowMuch);
//	vDebugOut("D mnX:%d mnY%d\n", mnPosX,mnPosY);
}

void MapScroller::vRightClick(sciEvent &oEvent)
{
	int nNodeX = ((oEvent.x / mnZoom) + (mnPosX / mnZoom) );
	int nNodeY = ((oEvent.y / mnZoom) + (mnPosY / mnZoom) );
//	vDebugOut ("newX:%d newy:%d\n", nNodeX,nNodeY);
	//MapNode *poNode = gpoMap->poGetNodeRC(nNodeX, nNodeY);
	MapNode *poNode = gpoMap->poClosestNode(nNodeX, nNodeY);
	if (poNode)
	{
		gpoMap->mpoPlane->vSetMovement(poNode);
	}
}

//////////////////////////////////
//								//
//		MapScrollerBox			//
//								//
//////////////////////////////////
MapScrollerBox::MapScrollerBox(BYTE jRed, BYTE jGreen, BYTE jBlue, int nThickness, int nZoom)
{
	mbMouseDown = FALSE;
	mwColor = (WORD)gpoBlender->dwPack(jRed, jGreen, jBlue);
	mnThickness = nThickness;
	mnZoom = nZoom;
	mpoBitmap = NULL;

	mnPrevMapPosX = 0;
	mnPrevMapPosY = 0;

	mnMinX = 0;
	mnMinY = 0;
	mnMaxX = 0;
	mnMaxY = 0;

	mnBitmapW = 0;
	mnBitmapH = 0;

	mnMapW = 0;
	mnMapH = 0;
	mnPlaneL = 0;
	mnPlaneT = 0;
	mnPlaneR = 0;
	mnPlaneB = 0;
	mnPlaneW = 0;
	mnPlaneH = 0;

	mnMouseScrollSpeed = game->mpoTemplate->lGetValue(TAG_Mscr,2);

//	vDebugOut("%d %d %d %d %d %d",	gpoMap->nGetSpacingX(), gpoMap->nGetSpacingY(),
//	gpoMap->nGetWidth(), gpoMap->nGetHeight(), gpoMap->nGetScreenHeight(),
//	gpoMap->nGetScreenWidth() );

}

MapScrollerBox::~MapScrollerBox()
{
	if (mpoBitmap)
	{
		delete mpoBitmap;
		mpoBitmap = NULL;
	}
}

void MapScrollerBox::vSetupBitmap()
{
	assert(plane);
	if (mpoBitmap)
		delete mpoBitmap;

	//store since we'll be referencing these a lot
	mnMapW = gpoMap->nGetWidth();
	mnMapH = gpoMap->nGetHeight();
	mnPlaneL = plane->GetX();
	mnPlaneT = plane->GetY();
	mnPlaneR = plane->GetRight();
	mnPlaneB = plane->GetBottom();
	mnPlaneW = plane->nWidth();
	mnPlaneH = plane->nHeight();

	//setup box's bitmap
	mnBitmapW = min(mnPlaneW - 1, gpoMap->nGetScreenWidth() * mnZoom + mnZoom - 1);	 //the -1's are needed for the off by 1 problem
	mnBitmapH = min(mnPlaneH - 1, gpoMap->nGetScreenHeight() * mnZoom + mnZoom - 1);
	mpoBitmap = new Bitmap (mnBitmapW, mnBitmapH, 16);

	//set up min/max values for box within plane
	mnMinX = 0;
	mnMinY = 0;
	mnMaxX = mnPlaneR - mnBitmapW;				//global for now
	mnMaxY = mnPlaneB - mnBitmapH;				//global for now
	plane->vGlobalToLocal(&mnMaxX, &mnMaxY);	//now localize
	
	// since we're a sub class of feature, need to set inital width and height this way
	left = 0;
	top = 0;
	right = mnBitmapW;
	bottom = mnBitmapH;

	SetRect(&mrRect, left, top, right, bottom);
	//mpoBitmap->vErase16(mrRect,jRed, jGreen, jBlue);
	mpoBitmap->vMakeRect(mwColor, mnThickness);

	mnPrevMapPosX = 0;
	mnPrevMapPosY = 0;
	Posn(0,0);
	
	mnPrevMousePosX = 0;
	mnPrevMousePosY = 0;
}

void MapScrollerBox::Posn(int posnX, int posnY)
{
	sciFeature::Posn(posnX,posnY);
	SetRect(&mrRect, left, top, right, bottom);
	if (inited && !hidden)
	{
		plane->window->Update(mrRect);
	}
}

void MapScrollerBox::Render (RECT &rUpdateRect)
{
	if (mpoBitmap)
		mpoBitmap->vDraw(left, top, plane,128);
}

Boolean MapScrollerBox::HandleEvent(sciEvent & oEvent)
{
	if ( OnMe(oEvent.x, oEvent.y) || mbMouseDown) {

		switch (oEvent.type) {
		case MOUSE_DOWN:
			if (oEvent.modifiers & LEFT_MOUSE)
			{
//				vLocalToGlobal(&oEvent.x, &oEvent.y);
				mnDragDiffX = abs(oEvent.x - left);	//difference of mouse position to the upper
				mnDragDiffY = abs(oEvent.y - top);	//left of bitmap
				mnNewX = left;						
				mnNewY = top;
				mbMouseDown = TRUE;
//				vGlobalToLocal(&oEvent.x, &oEvent.y);	
				oEvent.claimed = TRUE;
			}
			break; //MOUSE_DOWN

		case MOUSE_UP:
			mbMouseDown = FALSE;
			oEvent.claimed = TRUE;
			break; //MOUSE_UP

		case MOUSE_MOVE:
			if (mbMouseDown) {
//				vLocalToGlobal(&oEvent.x, &oEvent.y);
				oEvent.claimed = TRUE;

				mnNewX = oEvent.x - mnDragDiffX;
				mnNewY = oEvent.y - mnDragDiffY;

				if ((left != mnNewX) || (top != mnNewY) ) {
					vCheckPoints(mnNewX, mnNewY);
					left = mnNewX;
					top = mnNewY;
					bottom = top + mpoBitmap->Height();
					right = left + mpoBitmap->Width();
					Posn(left,top);
					vScrollGameMap();
					//vCalcClip();
				}
//				vGlobalToLocal(&oEvent.x, &oEvent.y);	
				plane->vMarkUpdate();
			}
			
			break; //MOUSE_MOVE

		}
		
	}	//bOnMe

//	if ( OnMe(oEvent.x, oEvent.y) ) 	
//		vDebugOut("on me\n");

//	vDebugOut("l:%d r:%d t:%d b:%d\n", left, right,top, bottom);

	return oEvent.claimed;
}


void MapScrollerBox::vCenter(int nX, int nY)
{
	//first position box
	int nNewX = nX - mnBitmapW / 2;
	int nNewY = nY - mnBitmapH / 2;
	vCheckPoints(nNewX, nNewY);
	Posn(nNewX, nNewY);
	//now adjust GameMap according to position
	vScrollGameMap();
}

// Takes 2 points and checks to see if the MapScrollerBox would fit its entire bitmap within
// its plane. On either point, if it any portion of the box would result in being clipped out of
// its plane, it adjusts the points accordingly setting them to the min/max allowed in order to
// keep the bitmap fully within the plane.
void MapScrollerBox::vCheckPoints(int &nNewX, int &nNewY)
{
	if (nNewX < mnMinX)					//check left edge
		nNewX = mnMinX;
	else if (nNewX > mnMaxX)			//check right edge
		nNewX = mnMaxX;

	if (nNewY < mnMinY)					//check top edge
		nNewY = mnMinY;
	else if (nNewY > mnMaxY)			//check bottom edge
		nNewY = mnMaxY;

}

void MapScrollerBox::Doit()
{

	if (!mbMouseDown)
	{
		vGameMapScrolled();	//check to see if gamemap was scrolled with cursor keys or mouse

	} else { 
		if (left == mnMaxX) {
			gpoMap->bScrollRight(mnMouseScrollSpeed);
			vGameMapScrolled();
		} else if (left == 0) {
			gpoMap->bScrollLeft(mnMouseScrollSpeed);
			vGameMapScrolled();
		}

		if (top == mnMaxY) {
			gpoMap->bScrollDown(mnMouseScrollSpeed);
			vGameMapScrolled();
		} else if (top == 0) {
			gpoMap->bScrollUp(mnMouseScrollSpeed);
			vGameMapScrolled();
		}

	}

}

//checks to see if gamemap has scrolled in any direction. If it has, self is repositioned within plane, and the minimap is 
//scrolled if need be
void MapScrollerBox::vGameMapScrolled() 
{
	int nCurMapPosX = gpoMap->nGetPosX();
	int nCurMapPosY = gpoMap->nGetPosY();

	if (mnPrevMapPosX != nCurMapPosX || mnPrevMapPosY != nCurMapPosY)
	{
		//gamemap was scrolled horizontally with either the cursor keys or the mouse at 
		//the edges of the screen
		int nX = nXScroll();
		int nY = nYScroll();
		mnPrevMapPosX = nCurMapPosX;
		mnPrevMapPosY = nCurMapPosY;
		vCheckPoints(nX, nY);
		Posn(nX, nY);
	}

}

//Scroll the game map according to position of self within plane
void MapScrollerBox::vScrollGameMap()
{
	int nNewX = (left - mnMinX + ((MapScroller*)plane)->nPosX()) / mnZoom;
	int nNewY = (top  - mnMinY + ((MapScroller*)plane)->nPosY()) / mnZoom;

	gpoMap->vPosn(nNewX, nNewY);

	mnPrevMapPosX = nNewX;
	mnPrevMapPosY = nNewY;

//	vDebugOut("nodeX:%d nodeY:%d\n", (int)(fNewX * mfMapW), (int)(fNewY * mfMapH));
}

int MapScrollerBox::nXScroll(void)
{
	int nCurMapPosX = gpoMap->nGetPosX();
	
	if (nCurMapPosX > mnPrevMapPosX)
	{
		//we scrolled to the right

		int nDiff = (nCurMapPosX - mnPrevMapPosX) * mnZoom;		//difference between current and previous (with magnification considered)
		if (left + nDiff > mnMaxX)
			((MapScroller*)plane)->vScrollRight(nDiff - (mnMaxX - left));
		return left + nDiff;	//be sure to run through vCheckPoints
	}
	
	else 
	if (nCurMapPosX < mnPrevMapPosX)
	{
		//we scrolled to the left
		
		int nDiff =  (mnPrevMapPosX - nCurMapPosX) * mnZoom;		//difference between previous and current (with magnification considered)
		if (left - nDiff < mnMinX)
			((MapScroller*)plane)->vScrollLeft(nDiff - (left - mnMinX));
		return left - nDiff;	//be sure to run through vCheckPoints
	}

	return left;

}

int MapScrollerBox::nYScroll(void)
{
	int nCurMapPosY = gpoMap->nGetPosY();

	if (nCurMapPosY > mnPrevMapPosY)
	{
		//we scrolled down

		int nDiff = (nCurMapPosY - mnPrevMapPosY) * mnZoom;		//difference between current and previous (with magnification considered)
		if (top + nDiff > mnMaxY)
			((MapScroller*)plane)->vScrollDown(nDiff - (mnMaxY - top));
		return top + nDiff;	//be sure to run through vCheckPoints

	}
	
	else 
	if (nCurMapPosY < mnPrevMapPosY)
	{
		//we scrolled up
		
		int nDiff =  (mnPrevMapPosY - nCurMapPosY) * mnZoom;		//difference between previous and current (with magnification considered)
		if (top - nDiff < mnMinY)
			((MapScroller*)plane)->vScrollUp(nDiff - (top - mnMinY));
		return top - nDiff;	//be sure to run through vCheckPoints

	}

	return top;

}



MapFrame::MapFrame(int nPri, Template *poInterfaceTemplate, MapScroller *poParent) : sciPlane(pApp->pWindow, FALSE, nPri)
{
	int nLeft = poInterfaceTemplate->lGetValue(TAG_MfrX,0);
	int nRight = poInterfaceTemplate->lGetValue(TAG_MfrY,0);
	int nWidth = poInterfaceTemplate->lGetValue(TAG_MfrW,0);
	int nHeight = poInterfaceTemplate->lGetValue(TAG_MfrH,0);

	vSetRect(0, 0, nWidth, nHeight);
	Posn(nLeft, nRight);
	mpoParent = poParent;
}

MapFrame::~MapFrame()
{
	mpoParent = NULL;
}

void MapFrame::vErase(Template *poInterfaceTemplate)
{
	int nRed = poInterfaceTemplate->lGetValue(TAG_MfrR,0);
	int nGreen = poInterfaceTemplate->lGetValue(TAG_MfrG,0);
	int nBlue = poInterfaceTemplate->lGetValue(TAG_MfrB,0);

	vSetBackgroundColor(nRed,nGreen,nBlue);
}

Boolean MapFrame::HandleEvent(sciEvent &oEvent)
{
//	return oEvent.claimed;
	if (isNotHidden == FALSE) {
		return FALSE;
	}


	//call the super
	sciPlane::HandleEvent(oEvent);
	
	//check for single clicks in the area
	if (!oEvent.claimed && OnMeGlobalized(oEvent.x, oEvent.y))
	{ 
		switch (oEvent.type) {
		case MOUSE_DOWN:
			mpoParent->vFrameEvent(oEvent);
			oEvent.claimed = TRUE;
			break;
		case MOUSE_MOVE:
//			vDebugOut("x: %d, y:%d\n", GetCursorX(), GetCursorY());
			oEvent.claimed = TRUE;
			break;
		}
	}

	return oEvent.claimed;

}
