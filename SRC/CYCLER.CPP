// CYCLER.H -- our cyclers
//
// TW
//


#include "cycler.h"
#include "unit.h"
#include "template.h"
#include "animationdb.h"
#include "game.h"
#include "procs.h"


Cycler::Cycler(DrawObj *poClient)
{
	assert(poClient);

	meDirection = CD_FORWARD;
	meType = CT_NONE;

	int nSpan = poClient->poTemplate()->lGetValue(TAG_Aspn, 0);
	switch (nSpan)
	{
		case 0:
			meSpan = CS_CONTINUOUS;
			break;
		case 1:
			meSpan = CS_INTERMITTENT;
			break;
		default:
			meSpan = CS_CONTINUOUS;
			break;
	}
	
	mpoClient = poClient;
	mbCycling = FALSE;
//	mpoClient->vStartCycling();

	mnGameCyclesPerSecond = 60 / game->nTicksPerGameCycle();
	mnCyclesPerFrame = poClient->poTemplate()->lGetValue(TAG_Acpf,3);
	mnCycleCtr = 1;
	
	mfMinWaitTime = poClient->poTemplate()->fGetValue(TAG_Anwt, 0.0);
	mfMaxWaitTime = poClient->poTemplate()->fGetValue(TAG_Axwt, 0.0);
	mfLoopsThru = poClient->poTemplate()->fGetValue(TAG_Alta, 1.0);

	//setup number of cels to animate and a delay time
	vReset();

}

Cycler::~Cycler()
{
	mpoClient->vStopCycling();
	mpoClient = NULL;
}

//Resets the number of cells to animate and the delay(if any) between animations
void Cycler::vReset()
{
	mnCycleCtr = 1;

	mnNumCels = mpoClient->nGetNumCels();
	mnLastCel = mnNumCels - 1;

	if (mfLoopsThru >= 1.0)
		mnCelsToAnimate = (int)mfLoopsThru * mnNumCels;
	else if (mfLoopsThru < 0.0)
		mnCelsToAnimate = abs( ((int)(mfLoopsThru * mnGameCyclesPerSecond)) / mnCyclesPerFrame );
	else
		mnCelsToAnimate = 0;
	mnCelsAnimated = 0;

	//now setup a new delay time
	vResetDelay();

	mpoClient->vSetCel(0);

}

//resets delay
void Cycler::vResetDelay()
{
	//select random range to the nearest tenth: xx.0 to xx.9
	//by temporarily shifting 1st decimal to the left so we can get an random integer
	int nMin = (int)(10 * mfMinWaitTime);
	int nMax = (int)(10 * mfMaxWaitTime);

	//now we can get a random integer range
	int nRand = GlobalRand(abs(nMax + 1 - nMin));

	//now move digit back to the right of decimal point and calc number of game cycles to delay
	int nCycle = (int)(((double)nRand / 10) * (double)mnGameCyclesPerSecond) + ((nMin * mnGameCyclesPerSecond) / 10) ;
	
	mnCyclesToDelay = nCycle;
	//mnCyclesToDelay = (mnMinWaitTime + GlobalRand(abs(mnMaxWaitTime + 1 - mnMinWaitTime))) * mnGameCyclesPerSecond;
	mnCyclesDelayed=0;
}

//Returns TRUE if we should be cycling, FALSE if we need to wait.
//	Note - always returns true if we are CONTINUOUS cycler
Boolean	Cycler::bTimeToCycle()
{
	if (meSpan == CS_CONTINUOUS)
		return TRUE;
	
	if (!mbCycling)
	{

		if (mnCyclesDelayed >= mnCyclesToDelay)
		{
			//time to start cycling
			mbCycling = TRUE;
			//reset for next delay
			vResetDelay();
			return TRUE;
		} else {
			//still waiting for delay to end
			++mnCyclesDelayed;
			return FALSE;
		}

	} else {
		//we're still cycling(haven't reach the cel limit yet) so it must be ok to continue
		return TRUE;
	}
}

//Returns TRUE if we're a continuous cycler or 
//
//Returns TRUE if we're intermittent and we havn't reach our cel limit and if we have, return FALSE and reset counter
Boolean Cycler::bChangeCel()
{
	//first need to adjust our frame rate
	if (mnCycleCtr % mnCyclesPerFrame != 0)
	{
		++mnCycleCtr;
		return FALSE;
	}
	mnCycleCtr = 1;

	
	if (meSpan == CS_CONTINUOUS)
		return TRUE;

	if (mnCelsAnimated >= mnCelsToAnimate)
	{
		//we finished all the cels we're supposed to cycle
		mbCycling = FALSE;
		mnCelsAnimated = 0;		//reset for next animation
		return FALSE;
	} else {
		++mnCelsAnimated;
		return TRUE;
	}
}

void Cycler::vAnimationCycle()
{

}




//////////////////////////////////////////////////////////////////
//																//
//																//
//		FORWARD CYCLER											//
//																//
//																//
//////////////////////////////////////////////////////////////////

ForwardCycler::ForwardCycler(DrawObj *poClient) : Cycler(poClient)
{
	meDirection = CD_FORWARD;
	meType = CT_FORWARD;
	goAnimationDB.vAddObject(this);
}

ForwardCycler::~ForwardCycler()
{
	goAnimationDB.vRemoveObject(this);
}

void ForwardCycler::vAnimationCycle()
{
	if (bTimeToCycle())
	{
		if (bChangeCel())
		{
			mpoClient->vIncrementCel();
			mpoClient->vMarkUpdate();
		}
	}
}




//////////////////////////////////////////////////////////////////
//																//
//																//
//		REVERSE CYCLER											//
//																//
//																//
//////////////////////////////////////////////////////////////////

ReverseCycler::ReverseCycler(DrawObj *poClient) : Cycler(poClient)
{
	meDirection = CD_REVERSE;
	meType = CT_REVERSE;
	goAnimationDB.vAddObject(this);

}

ReverseCycler::~ReverseCycler()
{
	goAnimationDB.vRemoveObject(this);
}

void ReverseCycler::vAnimationCycle()
{
	if (bTimeToCycle())
	{
		if (bChangeCel())
		{
			mpoClient->vDecrementCel();
			mpoClient->vMarkUpdate();
		}
	}
}





//////////////////////////////////////////////////////////////////
//																//
//																//
//		OSCILLATE CYCLER										//
//																//
//																//
//////////////////////////////////////////////////////////////////

OscillateCycler::OscillateCycler(DrawObj *poClient) : Cycler(poClient)
{
	meDirection = CD_FORWARD;
	meType = CT_OSCILLATE;
	mbWaitCycle = TRUE;
	goAnimationDB.vAddObject(this);

}

OscillateCycler::~OscillateCycler()
{
	goAnimationDB.vRemoveObject(this);
}

void OscillateCycler::vAnimationCycle()
{
	if (bTimeToCycle())
	{
		if (bChangeCel())
		{
			if (mnNumCels > 1)	//only need to oscillate bitmaps of 2 cels or larger
			{
				switch (meDirection)
				{
				case CD_FORWARD:
					if (mbWaitCycle) 
					{
						//we need to sit on start cel for one cycle
						mbWaitCycle = FALSE;
					} else { 
						if (mpoClient->nGetCel() + 1 >= mnLastCel)
						{
							meDirection = CD_REVERSE;
							mbWaitCycle = TRUE;
							mpoClient->vSetCel(mnLastCel);
						}
						else {
							mpoClient->vIncrementCel();
						}
					}

					mpoClient->vMarkUpdate();
					break;

				case CD_REVERSE:
					if (mbWaitCycle) 
					{
						//we need to sit on start cel for one cycle
						mbWaitCycle = FALSE;
					} else { 
						if (mpoClient->nGetCel() - 1 <= 0)
						{
							meDirection = CD_FORWARD;
							mbWaitCycle = TRUE;
							mpoClient->vSetCel(0);
						} else {
							mpoClient->vDecrementCel();
						}
					}
	
					mpoClient->vMarkUpdate();
					break;
				}
			}
		}
	}
}




//////////////////////////////////////////////////////////////////
//																//
//																//
//		RANDOM CYCLER											//
//																//
//																//
//////////////////////////////////////////////////////////////////

RandomCycler::RandomCycler(DrawObj *poClient) : Cycler(poClient)
{
	meDirection = CD_FORWARD;	//irrelevant
	meType = CT_RANDOM;
	goAnimationDB.vAddObject(this);

}

RandomCycler::~RandomCycler()
{
	goAnimationDB.vRemoveObject(this);
}

void RandomCycler::vAnimationCycle()
{
	if (bTimeToCycle())
	{
		if (bChangeCel())
		{
			if (mnNumCels > 1)	//only need to randomize bitmaps of 2 cels or larger
			{
				int nCel = GlobalRand(mnNumCels);
				while (nCel == mpoClient->nGetCel())
					nCel = GlobalRand(mnNumCels);
				mpoClient->vSetCel(nCel);
				mpoClient->vMarkUpdate();
			}
		}
	}
}




//////////////////////////////////////////////////////////////////
//																//
//																//
//		MOTION CYCLER (only for units)							//
//																//
//																//
//////////////////////////////////////////////////////////////////

MotionCycler::MotionCycler(DrawObj *poClient) : Cycler(poClient)
{
	meDirection = CD_FORWARD;
	meType = CT_MOVER;
	goAnimationDB.vAddObject(this);
	mpoUnit = (Unit*)poClient;
}

MotionCycler::~MotionCycler()
{
	goAnimationDB.vRemoveObject(this);
}

void MotionCycler::vAnimationCycle()
{
	if (bTimeToCycle())
	{
		if (bChangeCel())
		{
			mpoClient->vIncrementCel();
			mpoClient->vMarkUpdate();
		}
	}
}


//Always returns true if we aren't in a Fidget mode. If we are fidgeting, returns TRUE when we reach the end of the delay time(or if we havn't
//stopped cycling from a previous animation cycle). Returns FALSE if we are still waiting for delay time to finish
Boolean	MotionCycler::bTimeToCycle()
{
	ACTIONS eAction = mpoUnit->eAction();

	//We only need to delay if we are in a fidget mode, if we're not, then it's always time to cycle
	if (eAction != ACT_FIDGET_CLOAKED && eAction != ACT_FIDGET_UNCLOAKED)
		return TRUE;	//always time to cycle
	
	//we are fidgeting (either cloaked or uncloaked) so calculate delays between fidgets
	if (!mbCycling)
	{

		if (mnCyclesDelayed >= mnCyclesToDelay)
		{
			//time to start cycling
			mbCycling = TRUE;
			//reset for next delay
			vResetDelay();
			return TRUE;
		} else {
			//still waiting for delay to end
			++mnCyclesDelayed;
			return FALSE;
		}

	} else {
		//we're still cycling(haven't reach the cel limit yet) so it must be ok to continue
		return TRUE;
	}
}

Boolean MotionCycler::bChangeCel()
{
	//first need to adjust our frame rate
	if (mnCycleCtr % mnCyclesPerFrame != 0)
	{
		++mnCycleCtr;
		return FALSE;
	}
	mnCycleCtr = 1;

	
	ACTIONS eAction = mpoUnit->eAction();

	//We don't care how cels to animate when we walk or run or feed. We will continually cycle these actions.
	if (eAction == ACT_WALK || eAction == ACT_RUN  || eAction == ACT_FEED)
		return TRUE;	//always time change cel

	if (mnCelsAnimated >= mnCelsToAnimate)
	{
		//we finished all the cels we're supposed to cycle
		mbCycling = FALSE;
		mnCelsAnimated = 0;		//reset for next animation
		mpoClient->vCue();		//cue unit
		return FALSE;
	} else {
		++mnCelsAnimated;
		return TRUE;
	}
}

void MotionCycler::vReset()
{

	Cycler::vReset();
	
	//special case - When dying, only cycle to last cel of loop. Don't go back to cel 0
	if (mpoUnit->eAction() == ACT_DIE)
		mnCelsToAnimate = mnLastCel;
}