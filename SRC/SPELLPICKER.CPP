// SPELLPICKER.H -- the spell picker (or hand to hand)
//
// TW

#include "scimain.h"
#include "unit.h"
#include "statusln.h"
#include "spell.h"
#include "spellpicker.h"
#include "intplane.h"


SpellPicker::SpellPicker(int nPri, Template *poInterfaceTemplate, StatusLine *poStatusLine)
: sciPlane(pApp->pWindow, FALSE, nPri),
  moHandToHand(PICK_HAND), moCancel(PICK_CANCEL)
{
	assert(poStatusLine);
	poStatusLine->vClearBar();

	mpoStatusLine = poStatusLine;
	mpoTemplate = poInterfaceTemplate;
	mbShowHandToHand = FALSE;

	int nLeft = mpoTemplate->lGetValue(TAG_IspL,0);
	int nRight = mpoTemplate->lGetValue(TAG_IspR,0);
	int nTop = mpoTemplate->lGetValue(TAG_IspT,0);
	int nBottom = mpoTemplate->lGetValue(TAG_IspB,0);
	vSetRect(nLeft,nTop,nRight,nBottom);
//	vSetBackgroundColor(23,23,23);

	//initialize all possible spell icons and sort them by level
	int nNumSpells = game->mpoTemplate->lGetValue(TAG_gstc, 0);
	assert(nNumSpells);
	for (int i= 0; i < nNumSpells; ++i)
	{
		Spell *poSpell = game->moSpells.GetItem(i);
		PickerButton *poSpellBtn = new PickerButton(PICK_SPELL);
		poSpellBtn->vSetPlane(this);
		poSpellBtn->vSetStatusLine(mpoStatusLine);
		poSpellBtn->vSetStatusText(poSpell->GetName());
		poSpellBtn->SetView(poSpell->nGetPickerBitmapID());
		poSpellBtn->vSetSpellIndex(i);
		poSpellBtn->Init();
		poSpellBtn->SetButtonCels(0,2,3,1);
		poSpellBtn->SetPri(mnPriority + 100);
		poSpellBtn->Hide();

		//moSpells.AddToEnd(poSpellBtn);
		vAddSortedButton(poSpellBtn);
	}

	// hand to hand button
	int nX = mpoTemplate->lGetValue(TAG_IhhX,0);
	int nY = mpoTemplate->lGetValue(TAG_IhhY,0);
	moHandToHand.vSetPlane(this);
	moHandToHand.vSetStatusLine(mpoStatusLine);
	moHandToHand.vSetStatusText("Hand To Hand");
	moHandToHand.SetView(mpoTemplate->lGetValue(TAG_BPhh,0));
	moHandToHand.Init();
	moHandToHand.Posn(nX,nY);
	moHandToHand.SetButtonCels(0,2,3,1);
	moHandToHand.SetPri(mnPriority + 100);
	moHandToHand.Hide();


	//cancel button
	nX = mpoTemplate->lGetValue(TAG_IcnX,0);
	nY = mpoTemplate->lGetValue(TAG_IcnY,0);
	moCancel.vSetPlane(this);
	moCancel.vSetStatusLine(mpoStatusLine);
	moCancel.vSetStatusText("Cancel");
	moCancel.SetView(mpoTemplate->lGetValue(TAG_BIcn,0));
	moCancel.Init();
	moCancel.Posn(nX,nY);
	moCancel.SetButtonCels(0,2,3,1);
	moCancel.SetPri(mnPriority + 100);
	moCancel.Hide();


	//now calculate offsets and various starting positions
	PickerButton *poABtn= moSpells.GetItem(0);
	mnColOffset = poABtn->nBitmapWidth() + mpoTemplate->lGetValue(TAG_Icop,0);
	mnRowOffset = poABtn->nBitmapHeight() + mpoTemplate->lGetValue(TAG_Irwp,0);
	mnStartCol = mpoTemplate->lGetValue(TAG_Istc,0);
	mnStartRow = mpoTemplate->lGetValue(TAG_Istr,0);
	mnHandToHandX = mpoTemplate->lGetValue(TAG_IhhX,0);
	mnHandToHandY = mpoTemplate->lGetValue(TAG_IhhY,0);

	mnNumColumns = ((right - left) - mnStartCol) / mnColOffset; //calculate how many icons per row we can safely fit


	Hide();
}


SpellPicker::~SpellPicker() 
{
	mpoTemplate = NULL;
}


Boolean SpellPicker::HandleEvent(sciEvent & oEvent) 
{
	if (!isNotHidden) {
		return oEvent.claimed;
	}

	switch (oEvent.type)
	{
		case KEY_PRESS:
		{
			switch (oEvent.c) 
			{
				case VK_ESCAPE:
					oEvent.claimed = TRUE;
					vHideAndReturn();
					break;

				case VK_SPACE:
					vHideAndReturn();
					break;

			}
		}
	}

	sciPlane::HandleEvent(oEvent);
	return oEvent.claimed;
}

void SpellPicker::vShow(Boolean bShowHandToHand)
{
	int nCurCol = mnStartCol;
	int nColCount = 0;
	int nRowCount = 0;
	mbShowHandToHand = bShowHandToHand;

	mpoStatusLine->vClearBar();
	for (int i = 0; i < moSpells.GetSize(); ++i)
	{
		PickerButton *poBtn = moSpells.GetItem(i);
		if (nColCount >= mnNumColumns)
		{
			nCurCol = mnStartCol;
			nColCount = 0;
			++nRowCount;
		}
		if (game->poLocalPlayer()->jSpellKnown(poBtn->nSpellIndex()) != 0)
		{
			poBtn->Posn(nCurCol, mnStartRow + (mnRowOffset * nRowCount));
			nCurCol += mnColOffset;
			++nColCount;
			poBtn->Show();
		}
	}
	if (bShowHandToHand) {
		moHandToHand.Show();
	}
	moCancel.Show();
	sciPlane::Show();
}

void SpellPicker::Hide()
{
	for (int i = 0; i < moSpells.GetSize(); ++i)
	{
		PickerButton *poBtn = moSpells.GetItem(i);
		poBtn->Show();
	}
	moHandToHand.Hide();
	moCancel.Hide();
	sciPlane::Hide();
}

//adds button to moSpells and sorts by level of spell
void SpellPicker::vAddSortedButton(PickerButton *poBtn)
{
	PickerButton *tmpButton;
	PickerButton *prevTmpButton;
	int i;

	if (moSpells.GetSize() == 0)
	{
		moSpells.AddToEnd(poBtn);
		return;
	}

	// sort low to high
	i = 0;
	prevTmpButton = NULL;
	tmpButton = moSpells.At(i);
//	vDebugOut ("tmpLevel%d CurLevel%d\n", tmpButton->nSpellLevel(), poBtn->nSpellLevel() );
	while (tmpButton && (tmpButton->nSpellLevel() < poBtn->nSpellLevel()))
		{
			++i;
			prevTmpButton = tmpButton;
			tmpButton = moSpells.At(i);
		}
	
	if (prevTmpButton) {
		moSpells.AddAfter(prevTmpButton, poBtn);
	} else {
		moSpells.AddToEnd(poBtn);
	}
}

//hides self and returns to normal interface operation
void SpellPicker::vHideAndReturn()
{
	mpoStatusLine->vClearBar();
	Hide();
	gpoInterface->vShowButtons();
}






PickerButton::PickerButton(PICK_MODE eMode)
{
	meMode = eMode;
	mnSpellIndex = 0;
	mpoStatusLine = NULL;
	mpoStatusText = NULL;

}

PickerButton::~PickerButton() {
	if (mpoStatusText) {
		delete[] mpoStatusText;
		mpoStatusText = NULL;
	}

}

void PickerButton::vSetStatusText(const char *pstrNewText) {
	int nStrLen;

	if (mpoStatusText && pstrNewText && !strcmp(mpoStatusText, pstrNewText)) {
		return;
	}

	if (mpoStatusText) {
		delete[] mpoStatusText;
	}

	if (pstrNewText) {
		nStrLen = strlen(pstrNewText);
		mpoStatusText = new char[nStrLen + 1];
		strcpy(mpoStatusText, pstrNewText);
	}
}

Boolean PickerButton::HandleEvent(sciEvent &oEvent) {
	if (hidden) {
		return FALSE;
	}

	switch (oEvent.type) {

	case MOUSE_MOVE:
		if (OnMe(oEvent.x, oEvent.y)) {
			//vDebugOut("On me\n");
			//vDebugOut("x: %d, y:%d\n", plane->GetCursorX(), plane->GetCursorY());
			assert(mpoStatusLine);
			if (mpoStatusText) {
				mpoStatusLine->vAddToBar((PickerButton*)this, mpoStatusText);			
			}
//			oEvent.claimed = TRUE;
		} else {
			assert(mpoStatusLine);
			if (mpoStatusLine->bOnBar((PickerButton*)this)) {
				mpoStatusLine->vClearFromBar((PickerButton*)this);
			}
		}
		break;
	}

	return sciButton::HandleEvent(oEvent);
}


void PickerButton::DoLeftClick() {
	switch (meMode) 
	{
	case PICK_HAND:
		Unit::SetAttribute(NULL, ATTR_DEFAULT_ATTACK, (int)DA_HANDTOHAND);
		break;
	case PICK_SPELL:
		{
			if ( ((SpellPicker*)plane)->bShowHandToHand() )
			{
				// we got here through the default attack button
				Unit::SetAttribute(NULL, ATTR_DEFAULT_ATTACK, (int)DA_SPELL);
				Unit::SetAttribute(NULL, ATTR_READYSPELL, mnSpellIndex + 1); //need this +1 because Paul uses 1 as the first index, not 0
			} else {
				// we got here through the picker through the 'cast' button
				gpoMap->SetMode(UIMODE_CAST);
			}
		}
		break;
	case PICK_CANCEL:
		break;
	}

	((SpellPicker*)plane)->vHideAndReturn();
	
}

//returns the required level of the associated spell
int PickerButton::nSpellLevel()
{
	Spell *poSpell = game->GetTrueSpell(mnSpellIndex);
	assert(poSpell);
	if (poSpell) {
		return poSpell->GetRequiredLevel();
	} else {
		return 0;
	}
}