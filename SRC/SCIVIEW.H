#ifndef _sciview
#define _sciview

#include "scifeatr.h"

class Bitmap;
class ResourceManager;

class sciView : public sciFeature
	{
	public:
		sciView();
		~sciView();
		void Init();
		void SetView(int newView);
		void SetLoop(int newLoop);
		void SetCel(int newCel);
		int GetCel();
		int GetLoop();
		int GetView();
		int LastCel();
		int LastLoop();
		void Render(RECT &updateRect);
		void Posn(int posnX, int posnY);
		void Show();
		void Hide();
		Boolean OnMe(int x, int y);
		RECT *prGetClip() {return &rcWindow;}
		void vSetFlip(Boolean bFlip);
		int nBitmapWidth(void);
		int nBitmapHeight(void);

		// Someone else has to delete resourceManager;
		ResourceManager *resourceManager;

	protected:
		int view;
		int loop;
		int cel;
		Bitmap *bitmap;
		RECT rcWindow;
	};

#endif //_sciview
