#include "scimain.h"
#include "mapnode.h"
#include "mapplane.h"
#include "unit.h"
#include "vampire.h"
#include "spell.h"
#include "soundlist.h"
#include "procs.h"
#include "mob.h"

extern Missile * CreateMissile(Unit * poFromUnit, MapNode * poSourceNode, MapNode * poTargetNode, MISSILE_TYPE missile, int nWait, BOOL bSolid, float fPercent = 0.0, double X = 10.0, double Y = 10.0, int nFallOff = 2, int nDirection = 1);

Spell::Spell(Template & oClanTemplate):mpoTemplate(&oClanTemplate)
{
	meSpellType = (SPELL_TYPE)mpoTemplate->lGetValue(TAG_type, 1);
	for(int i=0; i<MAXSPELLPARAMCOUNT; i++)
	{
		dParam[i] = 0.0;
	}
}

SPELL_TYPE Spell::GetSpellType()
{
	return meSpellType;
}

UINT Spell::GetRange(Unit * poUnit)
{
	assert(poUnit);

	// SPELL RANGE COEFFICIENTS
	UINT A;
	UINT B;
	UINT C;
	UINT D;

	// BASE RANGE
	A = (UINT)mpoTemplate->lGetValue(TAG_rngA, 0);

	// SPELLCASTING LEVEL
	B = (UINT)mpoTemplate->lGetValue(TAG_rngB, 0);

	// CYCLES REQUIRED TO TRAVEL DISTANCE TO TARGET
	C = (UINT)mpoTemplate->lGetValue(TAG_rngC, 0);

	// VISIBLE RANGE
	D = (UINT)mpoTemplate->lGetValue(TAG_rngD, 0);

	if (A || B || C || D)
	{
		if (C)
		{
			if (mpoTargetUnit)
			{
				C = C * mpoTargetUnit->lDistToSpace(poUnit->nGetX(), poUnit->nGetY()) * mpoTemplate->lGetValue(TAG_sped, 0); 
			}
			else
			{
				C = 0;
			}
		}
		return A + B * (poUnit->nLevel() + poUnit->GetUpgradeSpells()) + C + D * poUnit->vGetVisibleRange();
	}
	else
		return 0;
}

UINT Spell::GetRequiredLevel()
{
	return (UINT)mpoTemplate->lGetValue(TAG_levl, 0);
}

float Spell::GetCost()
{
	return (float)mpoTemplate->fGetValue(TAG_powr, 0);
}

UINT Spell::GetCastResetAfterSpell()
{
	return (UINT)mpoTemplate->lGetValue(TAG_cras, 20);
}

float Spell::GetAdjustedSpellCost(Vampire * poVampire)
{
	assert(poVampire);

	float fCost = GetCost();

	int nSpells = poVampire->GetUpgradeSpells();
	Clan * poClan = (Clan*) (poVampire->vGetClan());

	if (nSpells == 1)
	{
		fCost = fCost * poClan->GetSpellCostCoefficient1();
	} 
	else if (nSpells == 2)
	{
		fCost = fCost * poClan->GetSpellCostCoefficient2();
	}
	return fCost;
}

void Spell::Cast_DeathRain()
{
	// LAUNCH FIREWORK SIMILARLY TO FIREBALL ARC'ING TO A POINT ABOVE TARGET
	Missile * poMissile = new Missile(MISSILE_DEATHFROMABOVE, gpoMap->mpoPlane, mpoSourceUnit, NULL, mpoTargetNode, 6000, 0, TRUE, 1.18f);

	//  HAVE IT BURST INTO N DOWNWARD PROJECTILES
	poMissile->SetImpact(IMPACT_DEATHRAIN);
	poMissile->SetSpell(this);
}

BOOL Spell::bNoTargetNeeded()
{
	return mpoTemplate->lGetValue(TAG_bNTN, 0);
}

BOOL Spell::bIsAttackUnitSpell()
{
	return mpoTemplate->lGetValue(TAG_bAUS, 0);
}

void Spell::Cast_DoomSpiral()
{
	int duration = GetDurationCycles();
	int numberofmissiles = mpoTemplate->lGetValue(TAG_par0, 5);
	int velocity = mpoTemplate->lGetValue(TAG_par1, 6);
	int nAngleIncrement = 360 / numberofmissiles;
	int nAngle = 0;
	int rotation = 360 / duration;

	// MAKE N SPIRAL STARTS
	for (int i = 0; i < numberofmissiles; i++)
	{
	// THAT SPIN OUTWARDS TO RANGE DECIMATING EVERYTHING IN THEIR PATHS 
		Missile * poMissile = new Missile(MISSILE_GROUNDSPIRAL, gpoMap->mpoPlane, mpoSourceUnit, mpoSourceUnit->poNode(), mpoSourceUnit->poNode(), 6010, 0, 0, 0);
		poMissile->SetDirection(nAngle);
		poMissile->SetVelocity(velocity);
		poMissile->SetRotation(rotation);
		poMissile->SetCyclesRemaining(duration);
		poMissile->SetSpell(this);
		nAngle += nAngleIncrement;
	}
}

void Spell::Cast_CallTheSun()
{
	int x, y;
	Missile * poMissile = new Missile(MISSILE_TIMER, gpoMap->mpoPlane, mpoSourceUnit, NULL, mpoTargetNode, 6008, 0, FALSE, 1.18f, NULL, 10.0, 10.0, 2, 0);
	poMissile->SetCyclesRemaining(20*(3 + 1 * mpoSourceUnit->nLevel()));
	poMissile->SetAlpha(64);
	
	x = mpoTargetNode->nPixelX();
	y = mpoTargetNode->nPixelY();
	poMissile->vSetPixelPos(x,y);
	poMissile->UpdateNode();
	poMissile = new Missile(MISSILE_TIMER, gpoMap->mpoPlane, mpoSourceUnit, NULL, mpoTargetNode, 6009, 0, FALSE, 1.18f, NULL, 10.0, 10.0, 2, 0);
	poMissile->SetCyclesRemaining(20*(3 + 1 * mpoSourceUnit->nLevel()));
	poMissile->SetAlpha(32);
	poMissile->vSetLayer(DP_FLOOR);
	y = y + 67;
	poMissile->vSetPixelPos(x,y);
	poMissile->UpdateNode();
	poMissile->SetSpell(this);
	
	// MAKE 
	//		AREA 5X5
	//		DAMAGE 10/CYCLE (FLAME/SMOKE WITH DAMAGE)

}

void Spell::Cast_LightningBolt()
{
	// MAKE LIGHTNING EFFECT STRIKE TARGETED UNIT
	Missile * poMissile;
	int n = GlobalRand(3);
	while (n)
	{
		poMissile = new Missile(MISSILE_INSTANT, gpoMap->mpoPlane, mpoSourceUnit, NULL, mpoTargetNode, 6006, 0, FALSE, 1.18f);
		poMissile->SetDamage(DAMAGE_NONE);
		n--;
	}
	poMissile = new Missile(MISSILE_INSTANT, gpoMap->mpoPlane, mpoSourceUnit, NULL, mpoTargetNode, 6006, 0, FALSE, 1.18f);
	poMissile->SetSpell(this);
	
	// TBD - MAKE FLASH EFFECT LIGHT UP WHOLE SCREEN
	// MAKE POOF! ON IMPACT
}

void Spell::Cast_Necromance()
{
	// CREATE UNITS FROM NOTHING OR DEAD UNITS
}

void Spell::Cast_VeilOfMist()
{
	// MAKE POOF!  VAMPIRE DISAPPEARS FROM LOCATION -- REPLACED BY HIGH SPEED MIST BALL --

	// CREATE A HOMING MISSILE
	MapNode * poSourceNode = mpoSourceUnit->poNode();

	// REMOVE VAMPIRE FROM MAP
	
	poSourceNode->mpoUnit = NULL;
	mpoSourceUnit->mpoCurNode = NULL;
	mpoSourceUnit->mpoPlane->vRemoveFromDrawnList(mpoSourceUnit);
	BOOL bWasSelected = mpoSourceUnit->bIsSelected();

	// CREATE THE MISSILE
	Missile * poMissile = new Missile(MISSILE_GROUNDHOMING, gpoMap->mpoPlane, NULL, poSourceNode, mpoTargetNode, 6013, 0, 0, 0);
	if (bWasSelected)
	{
		mpoSourceUnit->mpoPlane->mpoMap->vRemoveSelection(mpoSourceUnit);
		mpoSourceUnit->mpoPlane->mpoMap->vCommitSelection();
		poMissile->SetUnitWasSelected(bWasSelected);
	}
#define MISSILESPEED 3
	// HOW FAR CAN IT GO?
	poMissile->SetSpeed(1);
	poMissile->EmbedUnit(mpoSourceUnit);
	//poMissile->SetFlashEmbeddedUnitVisibility(TRUE);
	poMissile->SetSpell(this);
	mpoSourceUnit->vCommandNone();
	mpoSourceUnit->mbAlive = FALSE;
	poMissile->SetImpact(IMPACT_UNITREAPPEARS);

	// MOVES TO DESTINATION -- MIST DISSIPATES AND VAMPIRE REAPPEARS
}

void Spell::Cast_BrainWorm()
{
	if (mpoTargetUnit)
	{
	
		// ENABLE VISIBILITY OF WHERE THE HUMAN/OPPONENT IS LOOKING
		Missile * poMissile = new Missile(MISSILE_TIMER, gpoMap->mpoPlane, mpoSourceUnit, NULL, 0, 6000, 0, 0, 0, mpoTargetUnit);

		poMissile->vSetVisibility(FALSE);
		poMissile->SetCyclesRemaining(GetDurationCycles());
		poMissile->SetImpact(IMPACT_SPYOFF);
		poMissile->SetSpell(this);
		mpoTargetUnit->SetSpy(mpoSourceUnit->mpoOwner);
	}
}

void Spell::Cast_Phantoms()
{
	// CREATE PHANTOM UNIT ON TARGETED LOCATION
	if (mpoTargetNode && mpoTargetNode->mpoUnit == NULL && mpoSourceUnit->bIsVampire())
	{
		Vampire * poSourceVampire = (Vampire*)mpoSourceUnit;
		int nTemplate = poSourceVampire->mpoClan->GetTemplateIdForSexAndLevel(SEX_MALE, 1 + GlobalRand(mpoSourceUnit->nLevel() - 1));
		VampireDecoy * poDecoy = new VampireDecoy(mpoSourceUnit->poOwner(), mpoSourceUnit->mpoPlane, mpoTargetNode, nTemplate);
		poDecoy->SetUnitParentID(mpoSourceUnit->nGetID());
		pApp->pSoundManager->EmitFromPosition(poDecoy->nPixelX(), poDecoy->nPixelY(), SOUND_EnergyArrived);
	}
}

void Spell::Cast_MindCloud()
{
	// SCAN AREA FOR ENEMY UNITS
	//   RELAX EACH ENEMY UNIT
	UINT nDimension = 5;

	// ADJUST TO ODD VALUE IF NOT ZERO
	if (nDimension && (nDimension % 2 == 0)) nDimension++;

	int nMaxRange = nDimension / 2;

	int nStartX = max(0, mpoTargetNode->mnXPos - nMaxRange);
	int nStartY = max(0, mpoTargetNode->mnYPos - nMaxRange);
	int nEndX = min(gpoMap->nGetWidth()-1, mpoTargetNode->mnXPos + nMaxRange);
	int nEndY = min(gpoMap->nGetHeight()-1, mpoTargetNode->mnYPos + nMaxRange);

	for (int nX = nStartX; nX<nEndX; nX++)
	{
		for(int nY = nStartY; nY<nEndY; nY++)
		{
			MapNode * poNode = gpoMap->poGetNode(nX, nY);
			Vampire * poUnit = (Vampire *)poNode->mpoUnit;
			if (poUnit)
			{
				if (poUnit == mpoSourceUnit) continue;
				poUnit->vCommandNone();
				poUnit->Sequence_Clear();
				poUnit->ClearThought();
				if (poUnit->bIsVampire())
				{
					//		SET TENACITY TO SETTING_LOW
					//		SET AGGRESSION TO SETTING_LOW
					poUnit->SetAggression(SETTING_LOW);
					poUnit->SetTenacity(SETTING_LOW);
				}
			}
		}
	}
}

void Spell::Cast_EyeOfTheDragon()
{
	// START A LENS AT LOCATION
	// MOVE AWAY AT GIVEN SPEED FOR GIVEN TIME
}

void Spell::Cast_CallFog()
{
	// START A FOG BANK -- SEND OUT A FOGMAKERMAKER -- THE FOGMAKERS ARE EMITTED PERIODICALLY ORTHOGONALLY AND EMIT
	// FOG THAT GROWS--STAYS--THEN LIFTS
	Missile * poMissile = new Missile(MISSILE_FOG, gpoMap->mpoPlane, mpoSourceUnit, NULL, mpoTargetNode, 6007, 0, FALSE, 1.18f);
	poMissile->vSetVisibility(FALSE);
	poMissile->SetRotation(GetDamageRange(mpoSourceUnit));
	poMissile->SetSpeed(GetDurationCycles());
}

void Spell::Cast_Swap()
{
	if (mpoTargetUnit)
	{
		// IF TARGET UNIT IS OF LOWER LEVEL 
		if (mpoTargetUnit->nLevel() < mpoSourceUnit->nLevel())
		{
			//	THEN IF ENOUGH POWER
			//	SWAP UNITS
			pApp->pSoundManager->EmitFromPosition(mpoSourceUnit->nPixelX(), mpoSourceUnit->nPixelY(), SOUND_Teleport);
			mpoTargetUnit->vCommandNone();
			mpoTargetUnit->Sequence_Clear();
			mpoTargetUnit->ClearThought();
			mpoTargetUnit->mbBetweenPoints = FALSE;
			mpoTargetUnit->mnPartialMove = 0;
			MapNode * poNode1 = mpoTargetUnit->mpoCurNode;
			mpoTargetUnit->mpoCurNode = mpoSourceUnit->poNode();
			mpoSourceUnit->mpoCurNode->mpoUnit = mpoTargetUnit;
			mpoTargetUnit->vSetPixelPos(mpoSourceUnit->mpoCurNode->nPixelX(), mpoSourceUnit->mpoCurNode->nPixelY());
			mpoSourceUnit->mpoCurNode = poNode1;
			mpoSourceUnit->mpoCurNode->mpoUnit = mpoSourceUnit;
			mpoSourceUnit->vSetPixelPos(mpoSourceUnit->mpoCurNode->nPixelX(), mpoSourceUnit->mpoCurNode->nPixelY());

			//	ELSE 
			//	TELEPORT PARTWAY
		}
	}
}

void Spell::Cast_Call()
{
	if (mpoTargetUnit)
	{
		Command oCommand;

		// THRALL TARGET UNIT
		mpoTargetUnit->CommandSequenceClear();
		mpoTargetUnit->mnConvertCycleCount = GetDurationCycles();
		mpoTargetUnit->SetThrallOwner(mpoSourceUnit, GetDurationCycles());

		oCommand.meType = CM_MOVE;
		oCommand.mbTargetUnit = TRUE;
		oCommand.mnUnit = mpoSourceUnit->nGetID();
		
		// TELL TARGET UNIT TO APPROACH
		if (game->poLocalPlayer() == mpoSourceUnit->poOwner())
		{
			pApp->pSoundManager->EmitAmbient(SOUND_ComeToMe, SOUNDPRIORITY_UNITSOUNDS, FALSE, 128);
		}
		mpoTargetUnit->vCommand(oCommand, TRUE);
	}
}

void Spell::Cast_DeathFromAbove()
{
	MapNode * poSourceNode = mpoSourceUnit->poNode();
	Missile * poMissile;
	poMissile = new Missile(MISSILE_DEATHFROMABOVE, gpoMap->mpoPlane, mpoSourceUnit, poSourceNode, mpoTargetNode, 6000, 0, TRUE, 0);
	poMissile->SetImpact(IMPACT_DEATHFROMABOVE);
	poMissile->SetSpell(this);
}

void Spell::Cast_DisperseMob()
{
	if (mpoTargetUnit)
	{
		if (mpoTargetUnit->bIsMobMember())
		{
			mpoTargetUnit->mpoMobCoordinator->Disperse();
		}
	}
}

void Spell::Cast_ForceLeech()
{
	if (mpoTargetUnit)
	{
		// CREATE A HOMING MISSILE
		MapNode * poSourceNode = mpoSourceUnit->poNode();
		
		Missile * poMissile = new Missile(MISSILE_GROUNDHOMING, gpoMap->mpoPlane, mpoSourceUnit, poSourceNode, 0, 6014, 4, 0, 0, mpoTargetUnit);

		// HOW FAR CAN IT GO?
		poMissile->SetSpeed(mpoTemplate->lGetValue(TAG_sped, 2));
		poMissile->SetCyclesRemaining(GetDurationCycles());
			//poMissile->GetSpeed() * (min(50, 2*mpoTargetUnit->lDistToSpace(mpoSourceUnit->nGetX(), mpoSourceUnit->nGetY())) + 2 * mpoSourceUnit->nLevel()));
		poMissile->SetImpact(IMPACT_DRAINPOWERANDRETURN);
		poMissile->SetSpell(this);
		// RELEASE IT
	}
}

void Spell::Cast_Invisibility()
{
	mpoSourceUnit->SetInvisible();

	// ENABLE VISIBILITY OF WHERE THE HUMAN/OPPONENT IS LOOKING
	Missile * poMissile = new Missile(MISSILE_TIMER, gpoMap->mpoPlane, mpoSourceUnit, NULL, NULL, 6016, 0, 0, 0, mpoSourceUnit);
	poMissile->SetCyclesRemaining(GetDurationCycles());
	poMissile->SetImpact(IMPACT_CLEARINVISIBLE);
}

void Spell::Cast_GhostTouch()
{
	if (mpoTargetUnit)
	{
		mpoTargetUnit->SetFrozen();

		Missile * poMissile = new Missile(MISSILE_TIMER, gpoMap->mpoPlane, mpoSourceUnit, NULL, mpoTargetNode, 6011, mpoTargetUnit->nPixelX(), mpoTargetUnit->nPixelY(), 0, mpoTargetUnit);
		poMissile->SetAlpha(196);
		//poMissile->vSetVisibility(FALSE);
		int x = mpoTargetUnit->nPixelX();
		int y = mpoTargetUnit->nPixelY();
		poMissile->vSetPixelPos(x,y);
		poMissile->UpdateNode();


		poMissile->SetCyclesRemaining(10*(10 + mpoSourceUnit->nLevel()));
		poMissile->SetImpact(IMPACT_MELT);
	}
}

void Spell::Cast()
{
	// PLAY CAST SOUND
	int nSound = mpoTemplate->lGetValue(TAG_sn01, 0);
	if (nSound)
	{
		pApp->pSoundManager->EmitFromPosition(mpoTargetNode->nPixelX(), mpoTargetNode->nPixelY(), nSound, SOUNDPRIORITY_EXPLOSIONS);
	}

	switch (meSpellType)
	{
	case SPELL_DEATHRAIN:
		Cast_DeathRain();
		break;
	case SPELL_DOOMSPIRAL:
		Cast_DoomSpiral();
		break;
	case SPELL_CALLTHESUN:
		Cast_CallTheSun();
		break;
	case SPELL_LIGHTNINGBOLT:
		Cast_LightningBolt();
		break;
	case SPELL_NECROMANCE:
		Cast_Necromance();
		break;
	case SPELL_VEILOFMIST:
		Cast_VeilOfMist();
		break;
	case SPELL_BRAINWORM:
		Cast_BrainWorm();
		break;
	case SPELL_PHANTOMS:
		Cast_Phantoms();
		break;
	case SPELL_MINDCLOUD:
		Cast_MindCloud();
		break;
	case SPELL_EYEOFTHEDRAGON:
		Cast_EyeOfTheDragon();
		break;
	case SPELL_CALLFOG:
		Cast_CallFog();
		break;
	case SPELL_SWAP:
		Cast_Swap();
		break;
	case SPELL_CALL:			
		Cast_Call();
		break;
	case SPELL_DEATHFROMABOVE:
		Cast_DeathFromAbove();
		break;
	case SPELL_DISPERSEMOB:
		Cast_DisperseMob();
		break;
	case SPELL_FORCELEECH:
		Cast_ForceLeech();
		break;
	case SPELL_GHOSTTOUCH:
		Cast_GhostTouch();
		break;
	case SPELL_INVISIBILITY:
		Cast_Invisibility();
		break;
	default:
		assert(0);	// UNDEFINED SPELL TYPE
	}
}

void Spell::CastFromUnit(Unit * poSourceUnit, Unit * poTargetUnit, MapNode * poTargetNode)
{
	assert(mpoSourceUnit);
	assert(mpoTargetUnit || mpoTargetNode);

	if (!mpoTargetNode)
	{
		// LOAD THE TARGET NODE WITH SOMETHING
		if (mpoTargetUnit)
		{
			mpoTargetNode = mpoTargetUnit->poNode();
		}
		else if (mpoSourceUnit)
		{
			mpoTargetNode = mpoSourceUnit->poNode();
		}		
	}
	mpoSourceUnit = poSourceUnit;
	mpoTargetUnit = poTargetUnit;
	mpoTargetNode = poTargetNode;
	Cast();
}

UINT Spell::GetDurationCycles()
{
	assert(mpoSourceUnit);
	// DURATION = A + B * LEVEL + C * CYCLESTOCOVERDISTANCETOTARGET
	double A,B,C;

	A = mpoTemplate->fGetValue(TAG_durA, 0);
	B = mpoTemplate->fGetValue(TAG_durB, 0);
	C = mpoTemplate->fGetValue(TAG_durC, 0);
	double nDuration = 0;
	if (C)
	{
		assert(mpoTargetNode);
		int nDistance = mpoSourceUnit->lDistToSpace(mpoTargetNode->mnXPos, mpoTargetNode->mnYPos);
		nDuration = C * nDistance * mpoTemplate->lGetValue(TAG_sped, 2);
	}
	nDuration = 20 * (A + B * mpoSourceUnit->nLevel() + nDuration);
	if (nDuration < 0) nDuration = 0;
	return (UINT)nDuration;
}

float Spell::GetDamage(Unit * poUnit)
{
	assert(poUnit);
	double A,B;
	A = mpoTemplate->fGetValue(TAG_dmgA, 0);
	B = mpoTemplate->fGetValue(TAG_dmgB, 0);
	return (float)(A + B * poUnit->nLevel());
}

int Spell::GetDamageRange(Unit * poUnit)
{
	assert(poUnit);
	double A,B;
	A = mpoTemplate->fGetValue(TAG_dmrA, 0);
	B = mpoTemplate->fGetValue(TAG_dmrB, 0);
	return (int)(A + B * poUnit->nLevel());
}

long Spell::lCelCastDelay()
{
	return mpoTemplate->lGetValue(TAG_celc, 0);
}
