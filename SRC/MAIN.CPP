#include "os.h"
#include "version.h"
#include "app.h"
#include "resman.h"
#include "scigame.h"
#include "window.h"
#include "inifile.h"
#include "game.h"
#include <memory.h>
#include <string.h>
#include <stdio.h>
#include <direct.h>

// Exports

Application *pApp = NULL;
Game *game = NULL;
int numBytesAllocated = 0;
char versionText[32];
HINSTANCE hInstance = NULL;
BYTE btSoundBitSupport =0;
char cdDriveLetter = 0;
BOOL win32s = FALSE;
BOOL hdVersion = FALSE;
BOOL startNetNewGame = FALSE;
char *helpFilename = NULL;
char *resourcePaths[] = 
	{
	".",
	"resource",
	"..\\resource",
	"",
	};
int maxMemory = 32 * 1024 * 1024;
BOOL lowMemory = FALSE;
DWORD dwRAM = 0;

#ifdef HOYLE_NEW_GAME
char szIniFilename[] = "vampire.ini";
char *szAppName = "Vampire Wars : Inimicus";
char *szAppTitle = "Vampire Wars : Inimicus";
char *szResourceFilename = "vampire.prf";
char *shortHelpFilename = "vampire.hlp";
char *longHelpFilename = "..\\vampire.hlp";
char *movieFilename = "vampire.avi";
char *gsSaveDir = ".";
#endif // HOYLE_NEW_GAME

int Main();
extern void GetDXVersion(LPDWORD pdwDXVersion, LPDWORD pdwDXPlatform);

enum {
	ERROR_NONE,
	ERROR_WARNING,
	ERROR_SERIOUS,
	ERROR_FATAL
	};

#ifdef OS_WINDOWS

BOOL PreFlight() {
	HDC hDC;
	char szPrompt[255];
	char szTitle[255];
	WAVEOUTCAPS waveDevCaps;
	int iErrorLevel = ERROR_NONE;
	OSVERSIONINFO osvi;
	FILE *file;

	helpFilename = shortHelpFilename;
	file = fopen(helpFilename, "rb");
	if (file)
		{
		hdVersion = FALSE;
		fclose(file);
		}
	else
		{
		helpFilename = longHelpFilename;
		}
 	// check the Windows version.
	osvi.dwOSVersionInfoSize = sizeof(osvi);
	GetVersionEx(&osvi);
	if (osvi.dwPlatformId == VER_PLATFORM_WIN32s)
		win32s = TRUE;
	else
		win32s = FALSE;

	// Check for sound boards.
	waveOutGetDevCaps(0, &waveDevCaps, sizeof(waveDevCaps));
	if (waveDevCaps.dwFormats & WAVE_FORMAT_2M16) {
		btSoundBitSupport = 16;
		}
	else if (waveDevCaps.dwFormats & WAVE_FORMAT_2M08) {
		btSoundBitSupport = 8;
		}
	else
		btSoundBitSupport = 0;
	if (waveOutGetNumDevs()==0) {
		// No sound support.
		//wsprintf(szPrompt, "%s requires an installed sound driver.", szAppName);
		//iErrorLevel = ERROR_SERIOUS;
		}		
	else if (waveDevCaps.dwSupport & WAVECAPS_SYNC) {
		wsprintf(szPrompt, "The currently installed sound driver doesn't allow for asynchronous playback.");
		iErrorLevel = ERROR_SERIOUS;
		}

	// CHECK FOR DIRECTX 5 OR GREATER
	DWORD dwDXVersion, dwDXPlatform;
	GetDXVersion(&dwDXVersion, &dwDXPlatform);
	if (dwDXVersion < 0x500) 
	{
		wsprintf(szPrompt, "%s requires Microsoft� DirectX(tm) version 5 or above.", szAppName);
		iErrorLevel = ERROR_FATAL;
		goto lblShowError;
	}

	// Check for monitor support.
	hDC = GetDC(NULL);
	if ((GetDeviceCaps(hDC, HORZRES)<640) || (GetDeviceCaps(hDC, VERTRES)<480)) {
		wsprintf(szPrompt, "%s requires a screen resolution of 640x480 or larger.", szAppName);
		iErrorLevel = ERROR_SERIOUS;
		}

	if ((GetDeviceCaps(hDC, RASTERCAPS) & RC_PALETTE) && GetDeviceCaps(hDC, SIZEPALETTE) == 16) {
		wsprintf(szPrompt, "%s runs best when run in the video driver is set to 256 color mode.", szAppName);
		iErrorLevel = ERROR_WARNING;
		}
	ReleaseDC(NULL, hDC);

	// Check for memory.
	MEMORYSTATUS memoryStatus;

	memoryStatus.dwLength = sizeof(memoryStatus);
	GlobalMemoryStatus(&memoryStatus);
	dwRAM = memoryStatus.dwTotalPhys;

	// Reserve 4MB ram for the OS, but take at least 8MB.
	maxMemory = dwRAM - 4*1024*1024;
	if (maxMemory < 8*1024*1024)
		maxMemory = 8*1024*1024;
	if (dwRAM < 10*1024*1024)
		{
		lowMemory = TRUE;
		//wsprintf(szPrompt, "%s may need more memory than you have free.  Consider closing some other application.", szAppName);
		//iErrorLevel = ERROR_SERIOUS;
		}
	else
		{
		lowMemory = FALSE;
		}

	// Check for required files.
	FILE *pFile;
	char filename[255];
	HANDLE h;
	WIN32_FIND_DATA findFileData;
	IniFile *pIniFile;

	// ONLY FOR PROFILING!!!! -- DJM
	//_chdir("..");

	//GetCurrentDirectory(255, filename);

	h = FindFirstFile(szResourceFilename, &findFileData);
	if (h != INVALID_HANDLE_VALUE)
		{
		FindClose(h);
		}
	else
		{
		pIniFile = new IniFile(szIniFilename, "Settings");
		cdDriveLetter =	pIniFile->ReadValue("CD Drive", 0);
		delete pIniFile;
		strcpy(filename, "c:\\");
		strcat(filename, szResourceFilename);
		pFile = NULL;
		if (cdDriveLetter)
			{
			// Look in the previous drive.
			filename[0] = cdDriveLetter;
			pFile = fopen(filename, "rb");
			}
		if (!pFile)
			{
			filename[0] = 'c';
			pFile = fopen(filename, "rb");
			}
		while (pFile == NULL && filename[0] != 'z')
			{
			++filename[0];
			pFile = fopen(filename, "rb");
			}

		if (pFile != NULL)
			{
			// Found the CD.
			fclose(pFile);
			cdDriveLetter = filename[0];
			}
		else
			{
			iErrorLevel = ERROR_FATAL;
			wsprintf(szPrompt, "Cannot locate the %s CD. Insert the CD into the CD-Rom drive.", szAppName);
			}
		}
lblShowError:
	switch (iErrorLevel)
		{
		case ERROR_NONE:
			break;
		case ERROR_WARNING:
			wsprintf(szTitle, "%s - WARNING", szAppName);
			MessageBox(NULL, szPrompt, szTitle, MB_OK | MB_ICONINFORMATION | MB_TASKMODAL);
			break;
		case ERROR_SERIOUS:
			wsprintf(szTitle, "%s - %s ERROR", szAppName, "SERIOUS");
			strcat(szPrompt, "  Serious errors prevent certain aspects of ");
			strcat(szPrompt, szAppName);
			strcat(szPrompt, " from working.  Do you wish to continue?");
			if (IDYES == MessageBox(NULL, szPrompt, szTitle, MB_YESNO | MB_ICONQUESTION | MB_TASKMODAL))
				return TRUE;
			else
				return FALSE;
		case ERROR_FATAL:
			wsprintf(szTitle, "%s - %s ERROR", szAppName, "FATAL");
			strcat(szPrompt, "  Fatal errors prevent ");
			strcat(szPrompt, szAppName);
			strcat(szPrompt, " from running at all.");
			MessageBox(NULL, szPrompt, szTitle, MB_OK | MB_ICONSTOP | MB_TASKMODAL);
			return FALSE;
		}

	return TRUE;
	}

int PASCAL WinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPSTR lpszCmdParam, int )
{
	char szPrompt[64];

	if (hPrevInst)
		{
		wsprintf(szPrompt, "%s is already running.", szAppName);
		MessageBox(NULL, szPrompt, szAppName, MB_OK | MB_ICONSTOP | MB_TASKMODAL);
		return 0;
		}

	hInstance = hInst;

	// PB - INITIALIZE COM
	HRESULT hResult = CoInitialize(NULL);
	if (!SUCCEEDED(hResult)) 
	{
		MessageBox(NULL, "Failed to initialize COM library", szAppName, MB_OK | MB_ICONSTOP);
		return hResult;
	}

 	if (!_stricmp(lpszCmdParam, "-startNetNewGame"))
		startNetNewGame = TRUE;

	int nReturn = Main();

	// PB - UNINITIALIZE COM
	CoUninitialize();

	return nReturn;
}

#endif


int Main() {
	int iResult;

	if (!PreFlight())
		return 0;

	pApp = new Application();
	iResult = pApp->Run();
	delete pApp;

	return iResult;
	}

int maxMemoryAllocated = 0;

void *operator new(size_t s)
	{
	size_t *temp;

	// Free some memory if we're already using too much.
	// Too much is defined in VApp.
	if (pApp)
		pApp->PurgeResources();

	temp = (size_t *)malloc(s + 4);
	if ((temp == NULL) && pApp)
		{
		// Allocation failed, try to free some memory.
		pApp->PurgeResources(0);	// Purge all!

		// Try again.
		temp = (size_t *)malloc(s + 4);

		// Failure!
		if (temp == NULL)
			{
			MessageBox(NULL, "Out Of Memory!", "Hoyle New Game Error", MB_OK | MB_TASKMODAL);
			return NULL;
			}
		}
	*temp = s;
	numBytesAllocated += s;
	if (numBytesAllocated > maxMemoryAllocated)
		maxMemoryAllocated = numBytesAllocated;
	return temp+1;
	}

void operator delete(void *s)
	{
	size_t *temp;

	temp = (size_t*)s;
	--temp;
	numBytesAllocated -= *temp;
	free(temp);
	}
