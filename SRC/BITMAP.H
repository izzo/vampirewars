#ifndef _bitmap
#define _bitmap

#include "vbitmap.h"
#include "sciplane.h"

class ResourceManager;

class Bitmap : public VBitmap
	{
	public:
		Bitmap(WORD wNewWidth, WORD wNewHeight, int nBits = 8);
		Bitmap(int iResourceID, ResourceManager *newResourceManager = NULL, BOOL activatePalette = FALSE);
		Bitmap(RESOURCE_TYPES theType, char mask2, int iResourceID, ResourceManager *newResourceManager = NULL);
		~Bitmap();
		void CopyPixels(Bitmap *pDestBitmap, RECT rcDest, RECT rcSource, BOOL bTransparent) {
			VBitmap::CopyPixels(pDestBitmap, rcDest, rcSource, bTransparent);
		}
		void CopyPixels(HDC hdc, RECT rcDest, RECT rcSource);
		void CopyPixels(Window *pDestWindow, RECT rcDest, RECT rcSource);
		BYTE *GetData();
		void ActivatePalette();
		virtual void SetCel(int theCel);
		virtual void SetLoop(int theLoop);
		virtual int nGetCel();
		virtual int nGetLoop();
		virtual void vIncrementCel();
		virtual void vDecrementCel();
		virtual void vIncrementLoop();
		virtual void vDecrementLoop();
		virtual int GetNumLoops();
		virtual int GetNumCels();
		void DrawRect(COLORREF color, RECT rCurrentPos, sciPlane *poPlane, int nThickness = 1); //the color of the rect and destination plane to draw on
		void vDraw (int nX, int nY, sciPlane *poPlane, BYTE jAlpha = 255, DWCOLOR16 wRemapColor = 0, BYTE jRemapAmount = 0, RECT *prLocal = NULL, WORD *pwPalette = NULL);
		virtual short nGetOriginX(int nLoop = 0, int nCel = 0) {return 0;};
		virtual short nGetOriginY(int nLoop = 0, int nCel = 0) {return 0;};
		virtual void vSetOriginPoint(short nNewX, short nNewY, int nLoop = 0, int nCel = 0); 

		short nOriginX;
		short nOriginY;


	protected:
	};

#endif // _bitmap
