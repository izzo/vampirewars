// TEMPLATE.H -- Templates for game objects
//
// DJM

#ifndef _TEMPLATE_
#define _TEMPLATE_

#include "resource.h"
#include "tmpltags.h"
#include "prf.h"
#include "list.h"

#define FAST_C4ID_PACK

#ifdef FAST_C4ID_PACK
// this may cause alignment problems, but it's cheaper
// than the alternative. Works on 386 and up unless the
// Alignment Check bit is set
#define MAKEC4IDSTR(str) \
	(*(long*)str)
#else
#define MAKEC4IDSTR(str) \
	MAKEC4ID(str[0], str[1], str[2], str[3])
	//((long)((long)str[0] << 24 | (long)str[1] << 16 | (long)str[2] << 8 | (long)str[3]))
#endif

#define TEMPLATE_VERSION 1
#define TEMPLATE_MAGIC	 MAKEC4ID('t', 'm', 'p', 'l')

typedef struct _TAG_PAIR {
	unsigned long	ulTag;
	long			lData;
} TAG_PAIR;

typedef struct _TEMPLATE_HEAD {
	long	lMagic;
	long	lVersion;
	long	lListTagsBegin; // all of these in bytes from end of header
	long	lStringTagsBegin; 
	long	lListsBegin;
	long	lStringsBegin;
} TEMPLATE_HEAD;

class Template : public Resource 
{
public:
	Template(int nID);

	long lGetValue(long lTag, long lDefault);
	double fGetValue(long lTag, double fDefault);
	long *plGetList(long lTag, long *plSize); // default is NULL, size 0
	const char *strGetString(long lTag, char *strDefault);
	void vReload(void);	//reload the text or data if something has been changed

protected:
	void vScanTextFile(void);
	void vScanData(void);

	TAG_PAIR	*mpValueTags;
	int			mnValueTags;
	TAG_PAIR	*mpListTags;
	int			mnListTags;
	TAG_PAIR	*mpStringTags;
	int			mnStringTags;

	long		*mplLists;
	char		*mpchStrings;
};

#endif


			
				


			
				

	

