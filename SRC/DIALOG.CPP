// DIALOG.CPP
//
// TW

#include "dialog.h"


#define PIXELS_BELOW	10	// how many pixels below the title text should the input line appear
#define T_PAD			10	// top padding
#define B_PAD			10	// bottom padding
#define L_PAD			10	// left padding
#define R_PAD			10	// right padding

DialogPlane::DialogPlane(char *pstrTitleText, char *pstrInputData, int nNumCharacters) 
: sciPlane(pApp->pWindow, TRUE, 600)
{
//	vCalcClip();
//	vSetRect(100,100,200,200);
//	vSetRect(100,100,640,480);
//	Posn (100,100);
//	vSetBackgroundColor(17);
	vSetPri(pApp->pWindow->nGetHighPriority());

	mbDone = FALSE;
	mpstrData = NULL;
	mbValidData = FALSE;
	mnMaxChars = 0;
	memset (mstrTextBuffer, 0x0, MAX_INPUT);
	
	//setup and init the title text
	if (pstrTitleText != NULL) {
		mpoTitleText = new sciText();
		mpoTitleText->Posn(L_PAD,T_PAD);
//		mpoTitleText->SetHeight(20);
//		mpoTitleText->SetWidth(88);
		mpoTitleText->vSetPlane(this);
		mpoTitleText->SetText(pstrTitleText);
		mpoTitleText->vSetSize();
//		mpoTitleText->SetBackgroundColor(16);
//		mpoTitleText->SetJust(JUST_RIGHT | JUST_TOP);
		mpoTitleText->SetPri(mnPriority);
		mpoTitleText->Init();
	}
	else {
		mpoTitleText = NULL;
	}
	
	//setup and init the input line
	if (pstrInputData != NULL) {
		mpstrData = pstrInputData; 
		mnMaxChars = min(nNumCharacters, MAX_INPUT - 1); //set # of chars for input line
		mpoInputLine = new sciText();
		if (mpoTitleText) {
			mpoInputLine->Posn(L_PAD, PIXELS_BELOW + (mpoTitleText->GetY() + mpoTitleText->GetHeight()));
		}
		else {
			mpoInputLine->Posn(L_PAD, T_PAD);
		}
		mpoInputLine->vSetPlane(this);
		POINT oPnt;
		mpoInputLine->vGetSize(oPnt, mnMaxChars);
		mpoInputLine->SetWidth(oPnt.x);
		mpoInputLine->SetHeight(oPnt.y);
//		mpoInputLine->SetBackgroundColor(15);
		mpoInputLine->SetPri(mnPriority);
		mpoInputLine->Init();
	}
	else {
		mpoInputLine = NULL;
	}

}

DialogPlane::~DialogPlane() {
	if (mpoTitleText) {
		delete mpoTitleText;
		mpoTitleText = NULL;
	}
	if (mpoInputLine) {
		delete mpoInputLine;
		mpoInputLine = NULL;
	}
	pApp->pWindow->Update(mrClip);
}

Boolean DialogPlane::HandleEvent(sciEvent &oEvent)
{
	if (!oEvent.claimed) {
		switch (oEvent.type) {
		case MOUSE_DOWN:
//			vDebugOut ("x:%d y:%d\n", oEvent.x, oEvent.y);
			oEvent.claimed = TRUE;
			break;	//MOUSE_DOWN
		
		case KEY_PRESS:
			switch (oEvent.c) {
			
			case VK_RETURN:;
				mbDone = TRUE;
				if (mpoInputLine && (strlen(mpoInputLine->GetText()) != 0)) {
					mbValidData = TRUE;
					if (mpstrData != NULL) {
						strcpy(mpstrData, mstrTextBuffer);
					}
				}
				else {
					mbValidData = FALSE;
					mpstrData = NULL;
				}
				
				oEvent.claimed = TRUE;
				break;	//VK_RETURN

			case VK_ESCAPE:
				mbDone = TRUE;
				mbValidData = FALSE;
				oEvent.claimed = TRUE;
				break;	//VK_ESCAPE
			
			case VK_BACK:
				if (mpoInputLine && strlen(mpoInputLine->GetText()) > 0) {
					sprintf(mstrTextBuffer, "%s", mpoInputLine->GetText());
					mstrTextBuffer[strlen(mpoInputLine->GetText()) - 1] = 0;
//					vUpdate();
//					vDebugOut (">%s<\n", mpoInputLine->GetText());
				}
				oEvent.claimed = TRUE;
				break;	//VK_BACK

//			default:
//				oEvent.claimed = TRUE;
//				break;
			
			}	// switch
		
			break;	//KEY_PRESS

		case KEY_CHAR: 
			// Make sure the buffer doesn't overflow.
			if ( 
				mpoInputLine &&
				isprint ((char)oEvent.c) &&
				(strlen(mpoInputLine->GetText()) < sizeof(mstrTextBuffer) - 2) &&
				(strlen(mpoInputLine->GetText()) < mnMaxChars) 
			   ) {
				sprintf(mstrTextBuffer, "%s%c", mpoInputLine->GetText(), (char)oEvent.c);
				//vUpdate();
				oEvent.claimed = TRUE;
//					vDebugOut (">%s<\n", mpoInputLine->GetText());
			}
			//vDebugOut ("%c" ,oEvent.c);
			break; //KEY_CHAR
		}
	}

	if (oEvent.claimed)
	{
//		vDebugOut("str:%s\n",mstrTextBuffer);
		if (mpoInputLine)
			mpoInputLine->SetText(mstrTextBuffer);

		//a funky draw but need to do it this way because of the way we steal events
		//from windows
		vMarkUpdate();				//make window 'dirty'
		window->ForceUpdate();		//now draw our space
		//window->TruePaint(mrClip);
	}

	return oEvent.claimed;

}

void DialogPlane::Render(RECT & updateRect) {
	sciPlane::Render(updateRect);
//	pApp->pWindow->ForceUpdate();
/*	if (mpoTitleText) {
		mpoTitleText->vRefresh();
	}
	if (mpoInputLine) {
		mpoInputLine->SetText(mstrTextBuffer);
	}
*/}


void DialogPlane::vUpdate() {
	vMarkUpdate();				//make window 'dirty'
	window->ForceUpdate();		//now draw our space
	//Render(mrClip);
	//pApp->pWindow->ForceUpdate();
}

void DialogPlane::vAutoSize() {
	int nTitleWidth = 0, nTitleHeight = 0;
	int nInputWidth = 0, nInputHeight = 0;

	POINT oPoint;

	// get sizes of title and input

	if (mpoTitleText) {
		mpoTitleText->vGetSize(oPoint);
		nTitleWidth  = oPoint.x;
		nTitleHeight = oPoint.y;
	}

	if (mpoInputLine) {
		mpoInputLine->vGetSize(oPoint, mnMaxChars);
		nInputWidth  = oPoint.x;
		nInputHeight = oPoint.y;
	}

	//size plane
	int nNewWidth = (max(nTitleWidth, nInputWidth)) + L_PAD + R_PAD;	
	int nNewHeight = nTitleHeight + nInputHeight + PIXELS_BELOW + T_PAD + B_PAD;
	
	if ((!mpoTitleText) || (!mpoInputLine)) {
		nNewHeight -= PIXELS_BELOW;
	}

	vSetRect(0,0,nNewWidth, nNewHeight);

	//now position plane centered 
	int nScreenWidth = pApp->pWindow->Width();
	int nScreenHeight = pApp->pWindow->Height();
	Posn ((nScreenWidth - nNewWidth) / 2, (nScreenHeight - nNewHeight) / 2);
	
}



