#include "os.h"

#include "unihand.h"
#include "app.h"
#include "resman.h"

extern Application *pApp;
extern int numBytesAllocated;

UniHandle::UniHandle(DWORD dwNewSize)
	{
	pData = NULL;
	hData = NULL;
	iLockCount = 0;
	dwSize = 0;
	SetSize(dwNewSize);
	}

UniHandle::~UniHandle()
	{
	while (iLockCount > 0)
		Unlock();
	if (hData)
		#ifdef OS_WINDOWS
		GlobalFree(hData);
		#endif // OS_WINDOWS
		#ifdef THINK_CPLUS
		DisposHandle(hData);
		#endif
		numBytesAllocated -= dwSize;
	}

void *UniHandle::Lock()
	{
	if(hData == NULL)
		return NULL;
	if(!iLockCount++)
		{
		#ifdef OS_WINDOWS
		pData = GlobalLock(hData);
		#endif // OS_WINDOWS
		#ifdef THINK_CPLUS
		HLock(hData);
		pData = *hData;
		#endif
		}
	return (pData);
	}

void UniHandle::Unlock()
	{
	if(iLockCount == 1)
		{
		#ifdef OS_WINDOWS
		GlobalUnlock(hData);
		#endif // OS_WINDOWS
		#ifdef THINK_CPLUS
		HUnlock(hData);
		#endif
		pData = NULL;
		}
	if(iLockCount > 0)
		iLockCount--;
	}

DWORD UniHandle::Size()
	{
	return dwSize;
	}

HANDLE UniHandle::SetSize(DWORD dwNewSize)
	{
	HANDLE hRetVal;

	if (pApp && pApp->pResourceManager)
		pApp->PurgeResources();
	while (iLockCount)
		Unlock();
	if(dwNewSize == 0)
		{
		if (dwSize != 0)
			{
			#ifdef OS_WINDOWS
			GlobalFree(hData);
			#endif // OS_WINDOWS
			#ifdef THINK_CPLUS
			DisposHandle(hData);
			#endif
			hData = NULL;
			pData = NULL;
			numBytesAllocated -= dwSize;
			dwSize = 0;
			}
		}
	else {
		numBytesAllocated -= dwSize;
		if(dwSize == 0)
			{
			#ifdef OS_WINDOWS
			// GMEM_SHARE may be needed to play sounds.
			hRetVal = GlobalAlloc(GHND /* | GMEM_SHARE*/ , dwNewSize);
			if (hRetVal == 0)
				{
//				DebugString("UniHandle failed.  Purging Resources.\n");
				pApp->PurgeResources(0);	// Purge all!
				hRetVal = GlobalAlloc(GHND /* | GMEM_SHARE*/ , dwNewSize);
				if (hRetVal == 0)
					{
//					DebugString("UniHandle failed!\n");
					}
				}
			#endif // OS_WINDOWS
			#ifdef THINK_CPLUS
			hRetVal = NewHandleClear(dwNewSize);
			#endif
			}
		else {
			#ifdef OS_WINDOWS
			// GMEM_SHARE may be needed to play sounds.
			//hRetVal = GlobalReAlloc(hData, dwNewSize, GMEM_MODIFY);
			hRetVal = GlobalReAlloc(hData, dwNewSize, GMEM_ZEROINIT);
			if(hRetVal)
				hData = hRetVal;
			#endif // OS_WINDOWS
			#ifdef THINK_CPLUS
			SetHandleSize(hData, dwNewSize);
			hRetVal = MemError() == 0 ? hData : NULL;
			#endif
			}
		if(hRetVal)
			{
			dwSize = dwNewSize;
			numBytesAllocated += dwSize;
			hData = hRetVal;
			}
		}
	return hRetVal;
	}
