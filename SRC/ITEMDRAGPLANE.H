// ITEMDRAGPLANE.H -- the plane in which an inventory item will be dragged around on
//
// TW

#ifndef _itemdragplane_
#define _itemdragplane_

#include "sciplane.h"

class sciProp;
class Unit;

class ItemDragPlane : public sciPlane {
	
public:
	ItemDragPlane(int nPriority, int nItemBitmap, Unit *poUnit, int nItemIndex);
	~ItemDragPlane();
	Boolean HandleEvent(sciEvent & oEvent);
	void Doit();
protected:
	sciProp *mpoDragItem;
	Unit *mpoUnit;
	int mnItemIndex;
};


#endif
