// COMMAND.CPP -- command class
//
// DJM

#include "command.h"
#include "util.h"
#include "objectdb.h"
#include "gamemap.h"
#include "mapnode.h"
#include "drawobj.h"

#define PACKING_BYTE 0xCD

//
// PACKING/UNPACKING FUNCTIONS
//
// NOTE: We need to keep these parallel! Any change to one must be copied in the other!
//
Command::Command(BYTE *pBinaryCommand)
{
	assert(pBinaryCommand);

	// clear all fields first
	mbTargetUnit = FALSE;
	mnX = 0;
	mnY = 0;
	mnUnit = ID_NONE;
	mnAttributeID = ID_NONE;
	mnNewValue = 0;
	mnSelectSize = 0;
	mbInternal = FALSE;

	// now fill in important ones from binary command
	meType = (COMMAND_TYPE)pBinaryCommand[0];
	mnPlayer = (int)pBinaryCommand[1];

	switch (meType) {
	case CM_CLOAK:
	case CM_NONE:
		break;

	case CM_SELECT:
		{
			mnSelectSize = (int)pBinaryCommand[2];

			// 1 byte padding
			assert(pBinaryCommand[3] == PACKING_BYTE);

			int *pnIDs = (int*)&pBinaryCommand[4];

			int i;
			for (i = 0; i < mnSelectSize; ++i) {
				manSelection[i] = pnIDs[i];
			}
		}
		break;
	case CM_DROP:
	case CM_PATROL_WAYPOINT:
	case CM_GUARD_MOVEINTOPOSITION:
	case CM_GUARD_MOVEINTERNAL:
	case CM_GUARD_MOVETOVERIFY:
	case CM_MOVE:
	case CM_MOVE_INVITE:
	case CM_MOVE_FOLLOW:
		{
			// 2 bytes padding
			assert(pBinaryCommand[2] == PACKING_BYTE);
			assert(pBinaryCommand[3] == PACKING_BYTE);

			mnX = *(int*)(&pBinaryCommand[4]);
			mnY = *(int*)(&pBinaryCommand[8]);
			
			// used as parameter in guard movement for inside/outside
			
			mnUnit = *(int*)(&pBinaryCommand[12]);

			// TBD -- ID_NONE-1 is being used a big hack (my own) for determining whether this was
			// a player issued move into a building.  Just leave it until after this aug22 deadline.
			// and then CLEAN IT OUT!  -- PB
			if (mnUnit != ID_NONE && (mnUnit!=(ID_NONE-1)) && (CM_MOVE_INVITE == meType || CM_MOVE_FOLLOW == meType || CM_MOVE == meType))
			{
				mbTargetUnit = TRUE;
			}
			mnNewValue = *(int*)(&pBinaryCommand[16]);
		}
		break;
	case CM_CAST:
	case CM_GUARD:
	case CM_ATTACK:
		{
			mbTargetUnit = (BOOL)pBinaryCommand[2];

			// 1 byte padding
			assert(pBinaryCommand[3] == PACKING_BYTE);

			mnNewValue = *(int*)(&pBinaryCommand[4]);

			if (mbTargetUnit) 
			{
				mnUnit = *(int*)(&pBinaryCommand[8]);
			} 
			else 
			{
				mnX = *(int*)(&pBinaryCommand[8]);
				mnY = *(int*)(&pBinaryCommand[12]);
			}
		}
		break;
	case CM_GIVE:
	case CM_CONVERT:
	case CM_FEED:
	case CM_TAKE:
		{
			// 2 byte padding
			assert(pBinaryCommand[2] == PACKING_BYTE);
			assert(pBinaryCommand[3] == PACKING_BYTE);

			mnUnit = *(int*)(&pBinaryCommand[4]);
			mnNewValue = *(int*)(&pBinaryCommand[8]);
			mbTargetUnit = TRUE;
		}
		break;
	case CM_ATTR:
		{
			assert(pBinaryCommand[3] == PACKING_BYTE);

			mbTargetUnit = (BYTE)pBinaryCommand[2];
			mnAttributeID = *(int*)(&pBinaryCommand[4]);
			mnNewValue = *(int*)(&pBinaryCommand[8]);
			mnUnit = *(int*)(&pBinaryCommand[12]);
		}
		break;

	default:

		vDebugOut("Unknown command sent");

		assert(0);

		meType = CM_NONE;
		break;
	}
}

int Command::nCreateBinaryCommand(BYTE *pBinaryCommand)
{
	assert(pBinaryCommand);

	int nSize = 0;

	pBinaryCommand[0] = (BYTE)meType;
	pBinaryCommand[1] = (BYTE)mnPlayer;

	switch (meType) {
	case CM_CLOAK:
	case CM_NONE:
		nSize = 2;
		break;

	case CM_SELECT:
		{
			pBinaryCommand[2] = (BYTE)mnSelectSize;
			pBinaryCommand[3] = PACKING_BYTE;

			int *pnIDs = (int*)&pBinaryCommand[4];

			int i;
			for (i = 0; i < mnSelectSize; ++i) {
				pnIDs[i] = manSelection[i];
			}

			nSize = 4 + (mnSelectSize * sizeof(int));
		}
		break;

	case CM_DROP:		
	case CM_PATROL_WAYPOINT:
	case CM_GUARD_MOVEINTOPOSITION:
	case CM_GUARD_MOVETOVERIFY:
	case CM_GUARD_MOVEINTERNAL:
	case CM_MOVE:
	case CM_MOVE_INVITE:
	case CM_MOVE_FOLLOW:
		{
			// 2 bytes padding
			pBinaryCommand[2] = PACKING_BYTE;
			pBinaryCommand[3] = PACKING_BYTE;

			*(int*)(&pBinaryCommand[4]) = mnX;
			*(int*)(&pBinaryCommand[8]) = mnY;
			*(int*)(&pBinaryCommand[12]) = mnUnit;
			*(int*)(&pBinaryCommand[16]) = mnNewValue;

			nSize = 20;
		}
		break;
	case CM_CAST:
	case CM_GUARD:
	case CM_ATTACK:
		{
			pBinaryCommand[2] = (BYTE)mbTargetUnit;

			// 1 byte padding
			pBinaryCommand[3] = PACKING_BYTE;

			*(int*)(&pBinaryCommand[4]) = mnNewValue;

			if (mbTargetUnit) {
				*(int*)(&pBinaryCommand[8]) = mnUnit;
				nSize = 12;
			} else {
				*(int*)(&pBinaryCommand[8]) = mnX;
				*(int*)(&pBinaryCommand[12]) = mnY;
				nSize = 16;
			}
		}
		break;

	case CM_GIVE:
	case CM_CONVERT:
	case CM_FEED:
	case CM_TAKE:
		{
			// 2 byte padding
			pBinaryCommand[2] = PACKING_BYTE;
			pBinaryCommand[3] = PACKING_BYTE;

			*(int*)(&pBinaryCommand[4]) = mnUnit;
			*(int*)(&pBinaryCommand[8]) = mnNewValue;
			nSize = 12;
		}
		break;

	case CM_ATTR:
		{
			pBinaryCommand[2] = (BYTE)mbTargetUnit;
			pBinaryCommand[3] = PACKING_BYTE;

			*(int*)(&pBinaryCommand[4]) = (int)mnAttributeID;
			*(int*)(&pBinaryCommand[8]) = (int)mnNewValue;
			*(int*)(&pBinaryCommand[12]) = (int)mnUnit;

			nSize = 16;
		}
		break;

	default:

		vDebugOut("Need binary packer for command type %d", (int)meType);

		assert(0);

		pBinaryCommand[0] = (BYTE)CM_NONE;

		nSize = 2;
		break;
	}

	return nSize;
}

DrawObj *Command::poGetTarget(void)
{
	if (mbTargetUnit) {
		return goObjectDB.poGetObject(mnUnit);
	} else {
		return NULL;
	}
}

MapNode *Command::poGetTargetNode(void)
{
	if (mbTargetUnit) {
		DrawObj *poTarget = poGetTarget();
		if (poTarget) {
			return poTarget->poNode();
		} else {
			return NULL;
		}
	} else {
		return gpoMap->poGetNode(mnX, mnY);
	}
}

void Command::vGetTargetPos(int *pnX, int *pnY)
{
	if (mbTargetUnit) {
		DrawObj *poTarget = poGetTarget();
		if (poTarget) {
			MapNode *poNode = poTarget->poNode();
			*pnX = poNode->mnXPos;
			*pnY = poNode->mnYPos;
		} else {
			*pnX = -1;
			*pnY = -1;
		}
	} else {
		*pnX = mnX;
		*pnY = mnY;
	}
}

				



