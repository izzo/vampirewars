//
// File:	PATH.CPP
// Desc:	Graphics Engine - Path functions.
// Author:	Jim Napier, Don Munsil
// Date:	3/5/97
//
// Copyright (c) 1997 by Sierra On-Line, Inc.
// All Rights Reserved.
//

//
// Includes.
//
#include "path.h"
#include <math.h>

// define this to disable moving through a one-wide diagonal wall of obstacles
#define NO_CUT_CORNERS

#define FASTCALL __fastcall
#define INLINE	inline
#undef CHECK_LINKED_LIST


//
// Local functions.
//
//static int  OptimizePath(BYTE *map, const SIZE &map_size, POINT *points, int npoints);
//static BOOL HitsBoundary(BYTE *map, const SIZE &map_size, const POINT &p1, const POINT& p2);


#define MAX_CYCLES_SINCE_BETTER_PATH 120
const int kMaxCells      = 200;		// maximum active cells


//
// Macros for doing stuff
//
#define MOVE_DIST(poNodeFrom, poNodeTo)	(1)


///
/// POSSIBLE HEURISTICS
///

const int kHMult = 2;

// diagonal move equivalent to horiz
//#define H_DIST(dx, dy)			(max(abs(dx), abs(dy)) * kHMult) 

// call it the average of dx and dy
//#define H_DIST(dx, dy)				(((abs(dx) + abs(dy)) / 2) * kHMult) 

// must move horizontally
#define H_DIST(dx, dy)			((abs(dx) + abs(dy)) * kHMult)

// true distance
//#define H_DIST(dx, dy)			((int)(sqrt(dx*dx + dy*dy) * kHMult +.5))


//
// Special linked-list just for this job
//
#ifdef CHECK_LINKED_LIST
#define CELL_CHECK(a) a->vDebugCheck()
#else
#define CELL_CHECK(a)
#endif

class Cell {
public:
	MapNode *poNode;
	int		nNext;
	int		nPrev;
};

//#pragma optimize("agt", on)

class CellList {
public:
	// we'll allocate all we need in one swoop so as to
	// avoid lots of allocation overhead
	Cell  aoList[kMaxCells + 1]; // allow one for the head node
	int	  nUsed;
	int	  nFirstAvail;

	CellList() {
		memset(&aoList, 0, (kMaxCells + 1) * sizeof(Cell));
		nUsed = 0;
		nFirstAvail = 1;
	}
	int FASTCALL nFirstN(void) {return aoList[0].nNext;}
	int FASTCALL nNextN(int n) {return aoList[n].nNext;}
	BOOL FASTCALL bPushSorted(MapNode *poNode) {

		// we're out of space. For now, just return FALSE
		// and we'll give out the best available path
		if (nUsed >= kMaxCells) {
			// for the future -- possibly kill the least-likely
			// open node?

			return FALSE;
		}

		// find the spot to insert
		int n = 0;
		while (1) {
			int nNext = nNextN(n);
			if (!nNext || poNode->mnFDist <= aoList[nNext].poNode->mnFDist) {
				break;
			}
			n = nNext;
		}

		vInsertAfter(n, poNode);

		poNode->mbOpen = TRUE;
		//poNode->mbClosed = FALSE;

		CELL_CHECK(this);
		return TRUE;
	}
	MapNode *FASTCALL poPopFirst() {
		int n = aoList[0].nNext;

		// if list is empty, bail
		if (!n || !nUsed) {
			return NULL;
		}

		// grab the head and remove it
		MapNode *poRet = aoList[n].poNode;

		assert(poRet);
		vRemove(n);

		poRet->mbOpen = FALSE;

		CELL_CHECK(this);
		return poRet;
	}
	void FASTCALL vInsertAfter(int n, MapNode *poNode) {
		// insert
		aoList[nFirstAvail].poNode = poNode;
		aoList[nFirstAvail].nNext = aoList[n].nNext;
		aoList[nFirstAvail].nPrev = n;
		aoList[aoList[n].nNext].nPrev = nFirstAvail;
		aoList[n].nNext = nFirstAvail;
		++nUsed;

		// find the next available slot
		while(nFirstAvail < kMaxCells && aoList[nFirstAvail].poNode) {
			++nFirstAvail;
		}
	}

	void FASTCALL vRemove(int n) {
		assert(n && aoList[n].poNode && nUsed);

		// move it out of the list
		int nNext = aoList[n].nNext;
		int nPrev = aoList[n].nPrev;
		aoList[nNext].nPrev = nPrev;
		aoList[nPrev].nNext = nNext;

		// clear the old one
		aoList[n].poNode = NULL;
		aoList[n].nNext = 0;
		aoList[n].nPrev = 0;
		--nUsed;

		// reset first available if necessary
		if (n < nFirstAvail) {
			nFirstAvail = n;
		}
	}

	void FASTCALL vDebugCheck(void) {
		int n = nFirstN();
		int nCount = 0;
		int nLastF = 0;
		int nPrev = 0;
		while (n) {
			++nCount;
			MapNode *poNode = aoList[n].poNode;
			assert(poNode);
			assert(nLastF <= poNode->mnFDist);
			assert(aoList[n].nPrev == nPrev);
			nLastF = poNode->mnFDist;
			nPrev = n;
			n = nNextN(n);
		}
		assert(nCount == nUsed);
	}

	void FASTCALL vReSort(MapNode *poNode) {
		assert(poNode);
		assert(nUsed);
		assert(nFirstN());

		// we know the node must have a lower FDist, so we'll
		// find the place to stick it on the way to finding it
		int n = 0;
		int nInsert = 0;
		BOOL bFound = FALSE;
		do {
			int nNext = nNextN(n);
			if (!bFound && (!nNext || poNode->mnFDist <= aoList[nNext].poNode->mnFDist)) {
				nInsert = n;
				bFound = TRUE;
			}
			if (aoList[n].poNode == poNode) {
				break;
			}
			n = nNext;
		} while (n);

		// we should have found the correct spot on the way
		assert(bFound);
		if (n == nInsert) {
			return;
		}

		vRemove(n);
		vInsertAfter(nInsert, poNode);
		CELL_CHECK(this);
	}

};

/*
void CellList::vReSort(MapNode *poNode) {
	assert(poNode);
	assert(nUsed);
	assert(nFirstN());

	// we know the node must have a lower FDist, so we'll
	// find the place to stick it on the way to finding it
	int n = 0;
	int nInsert = 0;
	BOOL bFound = FALSE;
	do {
		int nNext = nNextN(n);
		if (!bFound && (!nNext || poNode->mnFDist <= aoList[nNext].poNode->mnFDist)) {
			nInsert = n;
			bFound = TRUE;
		}
		if (aoList[n].poNode == poNode) {
			break;
		}
		n = nNext;
	} while (n);

	// we should have found the correct spot on the way
	assert(bFound);
	if (n == nInsert) {
		return;
	}

	vRemove(n);
	vInsertAfter(nInsert, poNode);
	CELL_CHECK(this);
}
*/


// ortho first, then diag
//POINT  next_cell[8] = {{0,1},{1,0},{0,-1},{-1,0},{-1,-1},{-1,1},{1,1},{1,-1}};

// diag first, then ortho
//POINT  next_cell[8] = {{-1,-1},{-1,1},{1,1},{1,-1},{0,1},{1,0},{0,-1},{-1,0}};

// Clockwise
POINT  next_cell[8] = {{1,0},{1,1},{0,1},{-1,1},{-1,0},{-1,-1},{0,-1},{1,-1}};


//#pragma optimize("agt", on)

void vClearMap(GameMap *poMap)
{
	int nSizeX = poMap->nGetWidth();
	int nSizeY = poMap->nGetHeight();
	int i, j;

	for (i = 0; i < nSizeX; ++i) {
		for (j = 0; j < nSizeY; ++j) {
			MapNode *poNode = poMap->poGetNode(i, j);
			if (poNode->mpoParent) {
				poNode->mnHDist = 0; // perhaps precalculate H?
				poNode->mnGDist = 0;
				poNode->mnFDist = 0;
				poNode->mbOpen = FALSE;
				//poNode->mbClosed = FALSE;
				poNode->mpoParent = NULL;
			}
		}
	}
}

void vCheckForClear(GameMap *poMap)
{
	int nSizeX = poMap->nGetWidth();
	int nSizeY = poMap->nGetHeight();
	int i, j;

	for (i = 0; i < nSizeX; ++i) {
		for (j = 0; j < nSizeY; ++j) {
			MapNode *poNode = poMap->poGetNode(i, j);
			assert(!poNode->mpoParent);
		}
	}
}

void vClearTouched(MapNode *poNode)
{
	while (poNode) {
		MapNode *poTemp = poNode->mpoTouchedChain;
		poNode->mnHDist = 0; // perhaps precalculate H?
		poNode->mnGDist = 0;
		poNode->mnFDist = 0;
		poNode->mbOpen = FALSE;
		poNode->mpoParent = NULL;
		poNode->mpoTouchedChain = NULL;
		poNode = poTemp;
	}
}




//----------------------------------------------------------------------------
// FindPath... Find the shortest path from the start to the goal.  Returns
// the number of points if found, or 0 if a path was not found.  
//
// Uses an enhanced version of the A-Star algorithm described in "Smart Moves:
// Intelligent Path-Finding", in the Oct/Nov 96 issue of Game Developer.
//
BOOL bFindPath(GameMap *poMap, MapNode *poStart, MapNode *poGoal, 
			  RefList<MapNode> *poOut, MapNode **ppoDest)
{
	const int kCutoff        = 20000;	// maximum cells to search
	const int kHeuristic     = 2;		// scale factor for distance heuristic
	const int kMaxPathLen	 = 1000;	// maximum path we'll return
	int    j, nTempX, nTempY, dx, dy;
	int	   iterations;
	int	   nCyclesSinceBetterPath = 0;
	MapNode	*poDest;
	BOOL	bRet = FALSE;
	static int nCount =0;
	nCount++;
	assert(nCount == 1);
	#ifdef _DEBUG
	vCheckForClear(poMap);
	#endif

	CellList oOpenList;

	assert(poMap);
	assert(poStart);
	assert(poGoal);
	assert(poOut);
	assert(ppoDest);

	//
	// Initialize.
	//
	int nSizeX = poMap->nGetWidth();
	int nSizeY = poMap->nGetHeight();
	int nTotalNodes = nSizeX * nSizeY;

	// for now, make max iterations constant, but perhaps it
	// should depend on map size or something
	int nMaxIterations = kCutoff;

	// reset everything to 0
	//
	// now keeping track of just seen nodes
	//vClearMap(poMap);

	//
	// Initialize starting position.
	//
	int nGoalX = poGoal->mnXPos;
	int nGoalY = poGoal->mnYPos;
	int nCurX = poStart->mnXPos;
	int nCurY = poStart->mnYPos;
	MapNode *poNode = poStart;

	dx = nCurX - nGoalX;
	dy = nCurY - nGoalY;
	poNode->mnGDist = 0;
	poNode->mnHDist = H_DIST(dx, dy);
	poNode->mnFDist = poNode->mnGDist + poNode->mnHDist;
	poDest = poNode;

	MapNode *poChainHead = poNode;
	MapNode *poChainTail = poNode;

	//
	// Add starting cell.
	//
	oOpenList.bPushSorted(poNode);

	//
	// Find path.
	//
	for (iterations = 0; (iterations < nMaxIterations) && oOpenList.nUsed; iterations++) {

		//
		// break if we've passed our limit of iterations without
		// finding a better path
		//
		if (nCyclesSinceBetterPath > MAX_CYCLES_SINCE_BETTER_PATH) {
			break;
		}

		//
		// Find cell with the shortest distance to the goal.
		//
		// this is always the head of the list
		poNode = oOpenList.poPopFirst();
		assert(poNode);
		
		//
		// Check if we reached the goal.
		//
		if (poNode == poGoal) {

			//
			// We reached the goal.  Return the goal as the destination
			// and the shortest path.
			//

			bRet = TRUE;
			poDest = poGoal;
			goto exit;
		}

		nCurX = poNode->mnXPos;
		nCurY = poNode->mnYPos;

		//
		// Search for the shortest path starting with (nCurX, nCurY).  Next cell indices:
		//
		//    7  0  6
		//     \ | /     
		//      \|/
		//    3--*--1
		//      /|\
		//     / | \
		//	  4	 2  5
		//
		// Note that the horizonal and vertical direction are checked first. (Why?)
		//
		for (j = 0; j < 8; j++) {

			//
			// Get new map coordinates.
			//
			nTempX = nCurX + next_cell[j].x;
			nTempY = nCurY + next_cell[j].y;

			//
			// Avoid the border.
			//
			if ((nTempX < 0) || (nTempX >= nSizeX) || (nTempY < 0) || (nTempY >= nSizeY)) {
				continue;
			}

			MapNode *poTempNode = poMap->poGetNode(nTempX, nTempY);

			//
			// Get the cost of moving to this cell.
			//
			int nCost = MOVE_DIST(poNode, poTempNode);


			if (poTempNode->bBlocked()) {
				continue;			// check if hit impassable border
			}

#ifdef NO_CUT_CORNERS
			if (next_cell[j].x != 0 && next_cell[j].y != 0){
				// moving diagonally, make sure we don't have a diagonal wall

				// check horizontally adjacent
				MapNode *poCheck = poMap->poGetNode(nTempX, nCurY);
				BOOL bBlock = poCheck->bBlocked();

				// check vertically adjacent
				poCheck = poMap->poGetNode(nCurX, nTempY);

				// if both are full, we can't move this way
				if (bBlock && poCheck->bBlocked()) {
					continue;
				}
			}
#endif

			

			// calc g
			int nNewGDist = poNode->mnGDist + nCost;

			// if it's been visited and our cost is higher, continue
			if (poTempNode->mpoParent &&
				nNewGDist >= poTempNode->mnGDist) {
				continue;
			}

			//
			// tack it onto the touched list
			//
			if (!poTempNode->mpoTouchedChain && poTempNode != poChainTail) {
				poChainTail->mpoTouchedChain = poTempNode;
				poChainTail = poTempNode;
			}

			//
			// calc new data
			//
			if (!poTempNode->mnHDist) {
				dx = nTempX - nGoalX;
				dy = nTempY - nGoalY;
				poTempNode->mnHDist = H_DIST(dx, dy);
			}
			poTempNode->mnGDist = nNewGDist;
			poTempNode->mnFDist = poTempNode->mnGDist + poTempNode->mnHDist;
			poTempNode->mpoParent = poNode;

#ifdef _DEBUG
			{
				// check to make sure we've only moved 1 node
				int nDX = poNode->mnXPos - poTempNode->mnXPos;
				int nDY = poNode->mnYPos - poTempNode->mnYPos;
				assert(abs(nDX) <= 1);
				assert(abs(nDY) <= 1);
				assert(poNode != poTempNode);
			}
#endif

			// always keep track of the "best" node, so if we have to
			// bail, we can hand back the best path found so far
			if (poTempNode->mnHDist < poDest->mnHDist ||
				(poTempNode->mnHDist == poDest->mnHDist &&
				 poTempNode->mnGDist < poDest->mnGDist)) {
				poDest = poTempNode;
				nCyclesSinceBetterPath = 0;
			} else {
				++nCyclesSinceBetterPath;
			}

			// if this cell is an open one, we need to move it up the list
			if (poTempNode->mbOpen) {
				oOpenList.vReSort(poTempNode);
			} else {
				if (!oOpenList.bPushSorted(poTempNode)) {
					// ran out of node space -- return best
					bRet = FALSE;
					goto exit;
				}
			}
		} // for j

		// this node has been closed
		//poNode->mbClosed = TRUE;

		// can't be both closed and open
		//assert(!(poNode->mbClosed && poNode->mbOpen));
	}

	//
	// Path not found.
	//
	bRet = FALSE;

	// so at this point, we send back the node with the lowest F
	// and hopefully once the unit gets there, we can recalc
exit:

	// clear the output list
	poOut->RemoveAll();

	// we send the path back in "reverse" order, because our List
	// class works better adding to the end than to the front.
	// It's just as convenient to walk that way.
	poNode = poDest;
	iterations = 0;
	while (poNode != poStart) {
		assert(poNode);
		assert(++iterations < kMaxPathLen);
		poOut->AddToEnd(poNode);
#ifdef _DEBUG
		{
			// check to make sure we've only moved 1 node
			int nDX = poNode->mnXPos - poNode->mpoParent->mnXPos;
			int nDY = poNode->mnYPos - poNode->mpoParent->mnYPos;
			assert(abs(nDX) <= 1);
			assert(abs(nDY) <= 1);
			assert(poNode != poNode->mpoParent);
		}
#endif
		poNode = poNode->mpoParent;
	}

	*ppoDest = poDest;

	//
	// now clear all the touched nodes
	//
	vClearTouched(poChainHead);

#ifdef _DEBUG
	vCheckForClear(poMap);
#endif
	nCount--;
	return bRet;
}


