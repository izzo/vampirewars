// MINIMAP.cpp -- the minimap
//
// TW
//


#include "game.h"
#include "vampire.h"
#include "building.h"
#include "minimap.h"
#include "mapscroller.h"
#include "template.h"
#include "unit.h"

MiniMap::MiniMap(WORD wNewWidth, WORD wNewHeight, MapFrame *poPlane, int nZoom) 
 : Bitmap (wNewWidth * nZoom, wNewHeight * nZoom, 16)
{
	mbForcedUpdate = FALSE;
	mpoPlane = poPlane;
	mnZoom = nZoom;
	mnCycleUpdate = game->mpoTemplate->lGetValue(TAG_MMcu,2);

	mwBLACK = (WORD)gpoBlender->dwPack(4,4,4);
	
	mwNONE		= (WORD)gpoBlender->dwPack((BYTE)game->mpoTemplate->lGetValue(TAG_mnoR,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mnoG,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mnoB,0) );
	mwGRASS		= (WORD)gpoBlender->dwPack((BYTE)game->mpoTemplate->lGetValue(TAG_mgrR,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mgrG,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mgrB,0) );
	mwSAND		= (WORD)gpoBlender->dwPack((BYTE)game->mpoTemplate->lGetValue(TAG_msaR,0),(BYTE)game->mpoTemplate->lGetValue(TAG_msaG,0),(BYTE)game->mpoTemplate->lGetValue(TAG_msaB,0) );
	mwSNOW		= (WORD)gpoBlender->dwPack((BYTE)game->mpoTemplate->lGetValue(TAG_msnR,0),(BYTE)game->mpoTemplate->lGetValue(TAG_msnG,0),(BYTE)game->mpoTemplate->lGetValue(TAG_msnB,0) );
	mwOBSTACLE  = (WORD)gpoBlender->dwPack((BYTE)game->mpoTemplate->lGetValue(TAG_mobR,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mobG,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mobB,0) );
	mwBUILDINGO = (WORD)gpoBlender->dwPack((BYTE)game->mpoTemplate->lGetValue(TAG_mboR,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mboG,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mboB,0) );
	mwBUILDINGI = (WORD)gpoBlender->dwPack((BYTE)game->mpoTemplate->lGetValue(TAG_mbiR,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mbiG,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mbiB,0) );
	mwTREES		= (WORD)gpoBlender->dwPack((BYTE)game->mpoTemplate->lGetValue(TAG_mtrR,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mtrG,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mtrB,0) );
	mwSTONE		= (WORD)gpoBlender->dwPack((BYTE)game->mpoTemplate->lGetValue(TAG_mstR,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mstG,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mstB,0) );
	mwROADS		= (WORD)gpoBlender->dwPack((BYTE)game->mpoTemplate->lGetValue(TAG_mroR,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mroG,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mroB,0) );
	mwWATER		= (WORD)gpoBlender->dwPack((BYTE)game->mpoTemplate->lGetValue(TAG_mwaR,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mwaG,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mwaB,0) );
	mwBRIDGE	= (WORD)gpoBlender->dwPack((BYTE)game->mpoTemplate->lGetValue(TAG_mbrR,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mbrG,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mbrB,0) );
	mwGATE		= (WORD)gpoBlender->dwPack((BYTE)game->mpoTemplate->lGetValue(TAG_mgaR,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mgaG,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mgaB,0) );
	mwDIRT		= (WORD)gpoBlender->dwPack((BYTE)game->mpoTemplate->lGetValue(TAG_mdiR,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mdiG,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mdiB,0) );

	//tbd - get values from each individual palette
	/*
	mwPlayer1 = (WORD)gpoBlender->dwPack((BYTE)game->mpoTemplate->lGetValue(TAG_mp1R,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mp1G,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mp1B,0) );
	mwPlayer2 = (WORD)gpoBlender->dwPack((BYTE)game->mpoTemplate->lGetValue(TAG_mp2R,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mp2G,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mp2B,0) );
	mwPlayer3 = (WORD)gpoBlender->dwPack((BYTE)game->mpoTemplate->lGetValue(TAG_mp3R,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mp3G,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mp3B,0) );
	mwPlayer4 = (WORD)gpoBlender->dwPack((BYTE)game->mpoTemplate->lGetValue(TAG_mp4R,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mp4G,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mp4B,0) );
	mwPlayer5 = (WORD)gpoBlender->dwPack((BYTE)game->mpoTemplate->lGetValue(TAG_mp5R,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mp5G,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mp5B,0) );
	mwPlayer6 = (WORD)gpoBlender->dwPack((BYTE)game->mpoTemplate->lGetValue(TAG_mp6R,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mp6G,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mp6B,0) );
	mwPlayer7 = (WORD)gpoBlender->dwPack((BYTE)game->mpoTemplate->lGetValue(TAG_mp7R,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mp7G,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mp7B,0) );
	mwPlayer8 = (WORD)gpoBlender->dwPack((BYTE)game->mpoTemplate->lGetValue(TAG_mp8R,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mp8G,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mp8B,0) );

	maPlyrColors[0]= mwPlayer1;
	maPlyrColors[1]= mwPlayer2;
	maPlyrColors[2]= mwPlayer3;
	maPlyrColors[3]= mwPlayer4;
	maPlyrColors[4]= mwPlayer5;
	maPlyrColors[5]= mwPlayer6;
	maPlyrColors[6]= mwPlayer7;
	maPlyrColors[7]= mwPlayer8;
	*/

	//other colors
	mwVampireHunter = (WORD)gpoBlender->dwPack((BYTE)game->mpoTemplate->lGetValue(TAG_mvhR,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mvhG,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mvhB,0) );
	mwPeasant = (WORD)gpoBlender->dwPack((BYTE)game->mpoTemplate->lGetValue(TAG_mpeR,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mpeG,0),(BYTE)game->mpoTemplate->lGetValue(TAG_mpeB,0) );
	

	vErase();
}

MiniMap::~MiniMap()
{
}

void MiniMap::vErase()
{
	RECT rMine;
	SetRect(&rMine,0,0,wWidth,wHeight);
	//vErase16(rMine,200,0,0);
	vErase16(rMine,0,0,0);
	
}

void MiniMap::vSetPixel(POINT tWhere, WORD wColor)
{
	int i, j;

	// simple fall-through
	if (mnZoom == 1) {
		VBitmap::vSetPixel(tWhere, wColor);
		return;
	}

	for (i = 0; i < mnZoom; ++i)
	{
		for (j = 0; j < mnZoom; ++j)
		{
			POINT pnt = {i + (tWhere.x * mnZoom), j + (tWhere.y * mnZoom)};
			VBitmap::vSetPixel(pnt, wColor);
		}
	}
}

void MiniMap::vForceUpdate()
{
	mbForcedUpdate = TRUE;
	vGameCycle();
	mbForcedUpdate = FALSE;
}

void MiniMap::vDraw(int nX, int nY)
{
	//Bitmap::vDraw(nX * mnZoom, nY * mnZoom,mpoPlane);
	Bitmap::vDraw(nX, nY,mpoPlane);
}

void MiniMap::vGameCycle()
{
	if (mbForcedUpdate || game->nGameCycle() % mnCycleUpdate == 0)
	{
		int nMapW = gpoMap->nGetWidth();
		int nMapH = gpoMap->nGetHeight();
		WORD wObst = (WORD)gpoBlender->dwPack(255,255,255);
		WORD wInt = (WORD)gpoBlender->dwPack(172,172,172);
		WORD wUnit = (WORD)gpoBlender->dwPack(0,255,0);
		WORD wPsnt = (WORD)gpoBlender->dwPack(30,30,30);
		WORD wGnd = (WORD)gpoBlender->dwPack(149,123,89);

		int i,j;

		vErase();
		for (i = 0; i < nMapW; ++i) {
			for (j = 0; j < nMapH; ++j) {
			
				POINT tWhere = {i,j};
				BYTE jVis = gpoMap->jGetVisibility(i,j);
				MapNode *poNode = gpoMap->poGetNode(i,j);
				assert(poNode);

#ifdef _DEBUG
				if (jVis == 0)
					vSetPixel(tWhere, mwBLACK);
				else 
				{
/*					switch (poNode->eTerrainType()) 
					{
					case TT_CLEAR:
						vSetPixel(tWhere,wGnd);
						break;
					case TT_OBSTACLE:
						vSetPixel(tWhere, wObst);
						break;
					case TT_ENTRANCE:
						break;
					case TT_ROUGH:
						break;
					case TT_INTERIOR:
						vSetPixel(tWhere, wInt);
						break;

					}
*/		
					switch (poNode->eMiniMapTerrainType())
					{
						case MMTT_NONE:
							vSetPixel(tWhere, mwNONE);
							break;
						case MMTT_GRASS:
							vSetPixel(tWhere, mwGRASS);
							break;
						case MMTT_SAND:
							vSetPixel(tWhere, mwSAND);
							break;
						case MMTT_SNOW:
							vSetPixel(tWhere, mwSNOW);
							break;
						case MMTT_OBSTACLE:
							vSetPixel(tWhere, mwOBSTACLE);
							break;
						case MMTT_BUILDING:
							if (poNode->meType == TT_INTERIOR)
//								if (((Building*)poNode->poGetCover())->bIsOpen())
								if ( ((Building*)poNode->poGetCover()) && ((Building*)poNode->poGetCover())->bIsOpen())
									vSetPixel(tWhere, mwBUILDINGI);
								else
									vSetPixel(tWhere, mwBUILDINGO);
							else
								vSetPixel(tWhere, mwBUILDINGO);
							break;
						case MMTT_TREES:
							vSetPixel(tWhere, mwTREES);
							break;
						case MMTT_STONE:
							vSetPixel(tWhere, mwSTONE);
							break;
						case MMTT_ROADS:
							vSetPixel(tWhere, mwROADS);
							break;
						case MMTT_WATER:
							vSetPixel(tWhere, mwWATER);
							break;
						case MMTT_BRIDGE:
							vSetPixel(tWhere, mwBRIDGE);
							break;
						case MMTT_GATE:
							vSetPixel(tWhere, mwGATE);
							break;
						case MMTT_DIRT:
							vSetPixel(tWhere, mwDIRT);
							break;
					}


					
					
					if (poNode->mpoUnit)
					{
						switch (poNode->mpoUnit->eUnitType())
						{
						case UT_VAMPIRE:
							if (poNode->mpoUnit->bIsLocalVampire())
							{
								vSetPixel(tWhere,poNode->mpoUnit->poOwner()->wMiniMapColor());
								//vSetPixel(tWhere,wUnit);
							} else {
								//don't bother drawing unit if he's inside and the building isn't opened up
								if (poNode->meType == TT_INTERIOR && !((Building*)poNode->poGetCover())->bIsOpen() )
									break;

								//we're an enemy unit, so need to check marked status

								if (((Vampire*)poNode->mpoUnit)->bMarked(game->poLocalPlayer()->mnPlayer))
								{
									//the vampire has been marked by local player
									vSetPixel(tWhere,poNode->mpoUnit->poOwner()->wMiniMapColor());
								}
									
								else
								{
									// not marked yet
									vSetPixel(tWhere,mwPeasant);
								}
							}

							break;
						case UT_PEASANT:
							if (poNode->meType == TT_INTERIOR)
							{
								if (((Building*)poNode->poGetCover())->bIsOpen())
								{
									vSetPixel(tWhere,mwPeasant);
								}
							} else {
								vSetPixel(tWhere,mwPeasant);
							}
							break;
						case UT_GYPSY:
							break;
						case UT_TURK:
							break;
						case UT_SOLDIER:
							break;
						case UT_VAMPIREHUNTER:
							break;
						}
					}


				}


#else		
				WORD wBlend;

				if (jVis < MAP_VISIBILITY_EXPOSED_MAP) {
					vSetPixel(tWhere, mwBLACK);
					continue;
				}


				// see if there's a unit on the square
				if (jVis > MAP_VISIBILITY_EXPOSED_MAP) {
					if (poNode->mpoUnit)
					{
						switch (poNode->mpoUnit->eUnitType())
						{
						case UT_VAMPIRE:
							if (poNode->mpoUnit->bIsLocalVampire())
							{
								vSetPixel(tWhere,poNode->mpoUnit->poOwner()->wMiniMapColor());
								continue;
								//vSetPixel(tWhere,mwPlayer1);
							} else {
								//don't bother drawing unit if he's inside and the building isn't opened up
								if (poNode->meType == TT_INTERIOR && !((Building*)poNode->poGetCover())->bIsOpen() )
									break;

								//we're an enemy unit, so need to check marked status

								if (((Vampire*)poNode->mpoUnit)->bMarked(game->poLocalPlayer()->mnPlayer))
								{
									//the vampire has been marked by local player
									vSetPixel(tWhere, poNode->mpoUnit->poOwner()->wMiniMapColor());
									continue;
								}
									
								else
								{
									// not marked yet
									vSetPixel(tWhere, mwPeasant);
									continue;
								}

							}
							break;
						case UT_PEASANT:
							if (poNode->meType == TT_INTERIOR)
							{
								if (((Building*)poNode->poGetCover())->bIsOpen())
								{
									vSetPixel(tWhere,mwPeasant);
									continue;
								}
							} else {
								vSetPixel(tWhere,mwPeasant);
								continue;
							}
							break;
						case UT_GYPSY:
							break;
						case UT_TURK:
							break;
						case UT_SOLDIER:
							break;
						case UT_VAMPIREHUNTER:
							break;
						}

					} //if (poNode->mpoUnit)
				}
				

				// this is the fall-through -- no unit, not black
				switch (poNode->eMiniMapTerrainType())
				{
				
					case MMTT_NONE:
						wBlend = mwNONE;
						break;
					case MMTT_GRASS:
						wBlend = mwGRASS;
						break;
					case MMTT_SAND:
						wBlend = mwSAND;
						break;
					case MMTT_SNOW:
						wBlend = mwSNOW;
						break;
					case MMTT_OBSTACLE:
						wBlend = mwOBSTACLE;
						break;
					case MMTT_BUILDING:
						if (poNode->meType == TT_INTERIOR && ((Building*)poNode->poGetCover())->bIsOpen()) {
							wBlend = mwBUILDINGI;
						} else {
							wBlend = mwBUILDINGO;
						}
						break;
					case MMTT_TREES:
						wBlend = mwTREES;
						break;
					case MMTT_STONE:
						wBlend = mwSTONE;
						break;
					case MMTT_ROADS:
						wBlend = mwROADS;
						break;
					case MMTT_WATER:
						wBlend = mwWATER;
						break;
					case MMTT_BRIDGE:
						wBlend = mwBRIDGE;
						break;
					case MMTT_GATE:
						wBlend = mwGATE;
						break;
					case MMTT_DIRT:
					default:
						wBlend = mwDIRT;
						break;

				}
				
				

				jVis >>= 3;
				if (jVis == 16) {
					wBlend = (WORD)gpoBlender->dwCalcGray50(wBlend);
				} else {
					wBlend = (WORD)gpoBlender->dwCalcAlphaBlack5(wBlend, jVis);
				}
				vSetPixel(tWhere, wBlend);
				
#endif
				

			}
		}


		//vDebugOut("cycle:%d\n",game->nGameCycle());
	}

	mpoPlane->vMarkUpdate();

}
