#include "Unit.h"
#include "objectdb.h"
#include "Vampire.h"
#include "human.h"
#include "HumanVampireHunter.h"


void HumanVampireHunter::vSetLevel(int nLevel)
{
	assert(nLevel<=4);
	assert(nLevel>0);

	nLevel = max(0, min(nLevel, 4));
	mnLevel = nLevel;
	// manUpgrade[UPGRADE_Level] = nLevel;

	vUpdateAttributes();
}

// TBD - only recalculate attributes when dependencies change
void HumanVampireHunter::vUpdateAttributes()
{
	mnAttackSpeed = vGetClanTemplate()->lGetValue(TAG_aspd, 20) - (mnLevel + (GetUpgradeSpeed() * 2));
	SetSpeed(SPEED_NORMAL);

	// 7 // TBD - PULL FROM CLAN TEMPLATE
	// TBD - PULL FROM COMMON PEASANT CLAN NORMAL SPEED

	mfMaxPower = Vampire::GetPowerForLevel(mnLevel) * (float)vGetClan()->GetPowerCoefficient();
	mfMaxHealth = Vampire::GetHealthForLevel(mnLevel) * (float)vGetClan()->GetHealthCoefficient();;
}

int HumanVampireHunter::vGetVisibleRange()
{
	return mnLevel + GetUpgradeSight() + vGetClan()->GetBaseSight();
}


HumanVampireHunter::HumanVampireHunter(Player *poOwner, MapPlane *poPlane, MapNode *poNode, int nTemplateID):Human(poOwner, poPlane, poNode, nTemplateID)
{
	// SETUP VAMPIRE HUNTER SPECIFICS
	vSetLevel(mnLevel);

	// DEFAULT POWER AND HEALTH TO MAXIMUM SETTING
	mfPower = mfMaxPower;
	mfHealth = mfMaxHealth;

	// SPEED SETTING SHOULD BE NORMAL SPEED
	SetSpeed(SPEED_NORMAL);

	//  VISIBLE RANGE
	mnVisibleRange = vGetVisibleRange();
	mnVampireRange[0] = mnVisibleRange;
	mnVampireRange[1] = mnVisibleRange;
	mnVampireRange[2] = mnVisibleRange;
}

void HumanVampireHunter::vThink()
{
	switch(moCurrentCommand.meType)
	{
	case CM_ATTACK:
	case CM_CAST:
		break;
	default:
		{
			BOOL bVampiresInVisibleRange = FALSE;
            int i;
			for (i=0;i<game->nGetPlayerCount();i++)
			{
				if (mnVisibleTotal[i].count)
				{
					bVampiresInVisibleRange = TRUE;
					break;
				}
			}
			if (bVampiresInVisibleRange)
			{
				int n = moVisibleUnits.GetSize();
				while(n)
				{
					n--;
					Vampire * poUnit = (Vampire *)(goObjectDB.poGetObject(moVisibleUnits.GetItem(i)));
					if (poUnit == NULL) continue;
					if (poUnit->bIsVampire() && !poUnit->bCloaked())
					{
						Command oCommand;
						oCommand.mbTargetUnit = TRUE;
						oCommand.mnUnit = poUnit->nGetID();
						oCommand.meType = CM_ATTACK;
						vCommand(oCommand, TRUE);
						break;
					}
				}
			}
		}
	}
	Unit::vThink();
}

void HumanVampireHunter::vLeaveMobIfNecessary()
{
	return;
}

void HumanVampireHunter::vFormAMobIfNecessary()
{
	return;
}