Vampire Wars
------------

This is the source code and game data for Vampire Wars, an unreleased
prototype by Sierra.

The original release can be found here:

https://archive.org/details/vampirewars_unreleased

This repository contains modifications to allow the game to build and run
under Visual Studio 2019 with Platform Toolset v142. To run it under Visual
Studio, remember to set your working directory to $(ProjectDir)..\GAME

Rendering
---------

The original game required a 16 bit per pixel (bpp) desktop video mode.
However, these days Windows no longer supports 16 bpp. To get around this,
an offscreen 32 bpp DIB is created. The game's internal pixel masks are then
forced to 555, so as far as the game is concerned it is rendering to a 16 bpp
555 format framebuffer. In `vFrameDone` before the DIB is displayed on the
device context, the 16 bpp framebuffer is expanded to 32 bpp.

Game data
---------

Two small changes were made to the game data to get the game to run. It is
not yet clear why these changes were required; the pre-built binary in the
GAME directory works without these changes.

1)
The global spell template count (gstc) was set to 21 in the 1.tlt template.
This controls the number of spells loaded at startup. Without this, the game
fails to load any spells. The value '21' was used because there are some
parts of the code that seem to assume a default value of 21 spells.

2)
In spell template 61 (61.tlt), the tag BPsp was set to 620. This sets the
spell's bitmap id. This is used by the `SpellPicker` class when creating
`PickerButton` instances for spells. Without a bitmap set, there is a divide
by zero which crashes the game.

